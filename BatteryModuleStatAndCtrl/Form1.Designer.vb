﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CmbSerialPorts = New System.Windows.Forms.ComboBox()
        Me.TxtSerialPrt = New System.Windows.Forms.TextBox()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TxtBaudRate = New System.Windows.Forms.TextBox()
        Me.CmbBaudRate = New System.Windows.Forms.ComboBox()
        Me.BtnComPrt = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'CmbSerialPorts
        '
        Me.CmbSerialPorts.FormattingEnabled = True
        Me.CmbSerialPorts.Location = New System.Drawing.Point(649, 29)
        Me.CmbSerialPorts.Name = "CmbSerialPorts"
        Me.CmbSerialPorts.Size = New System.Drawing.Size(121, 21)
        Me.CmbSerialPorts.TabIndex = 2
        '
        'TxtSerialPrt
        '
        Me.TxtSerialPrt.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TxtSerialPrt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtSerialPrt.ForeColor = System.Drawing.Color.Black
        Me.TxtSerialPrt.Location = New System.Drawing.Point(649, 12)
        Me.TxtSerialPrt.Name = "TxtSerialPrt"
        Me.TxtSerialPrt.Size = New System.Drawing.Size(100, 13)
        Me.TxtSerialPrt.TabIndex = 3
        Me.TxtSerialPrt.Text = "Select Serial Port"
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(61, 4)
        '
        'TxtBaudRate
        '
        Me.TxtBaudRate.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TxtBaudRate.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtBaudRate.ForeColor = System.Drawing.Color.Black
        Me.TxtBaudRate.Location = New System.Drawing.Point(649, 56)
        Me.TxtBaudRate.Name = "TxtBaudRate"
        Me.TxtBaudRate.Size = New System.Drawing.Size(100, 13)
        Me.TxtBaudRate.TabIndex = 5
        Me.TxtBaudRate.Text = "Select Baud Rate"
        '
        'CmbBaudRate
        '
        Me.CmbBaudRate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.CmbBaudRate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CmbBaudRate.FormattingEnabled = True
        Me.CmbBaudRate.Items.AddRange(New Object() {"9600", "19200", "38400", "57600", "115200", "230400", "312500"})
        Me.CmbBaudRate.Location = New System.Drawing.Point(649, 75)
        Me.CmbBaudRate.Name = "CmbBaudRate"
        Me.CmbBaudRate.Size = New System.Drawing.Size(121, 21)
        Me.CmbBaudRate.TabIndex = 6
        '
        'BtnComPrt
        '
        Me.BtnComPrt.Location = New System.Drawing.Point(649, 111)
        Me.BtnComPrt.Name = "BtnComPrt"
        Me.BtnComPrt.Size = New System.Drawing.Size(75, 23)
        Me.BtnComPrt.TabIndex = 7
        Me.BtnComPrt.Text = "Button1"
        Me.BtnComPrt.UseVisualStyleBackColor = True
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.BtnComPrt)
        Me.Controls.Add(Me.CmbBaudRate)
        Me.Controls.Add(Me.TxtBaudRate)
        Me.Controls.Add(Me.TxtSerialPrt)
        Me.Controls.Add(Me.CmbSerialPorts)
        Me.Name = "FrmMain"
        Me.Text = "Battery Module"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents CmbSerialPorts As ComboBox
    Friend WithEvents TxtSerialPrt As TextBox
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents TxtBaudRate As TextBox
    Friend WithEvents CmbBaudRate As ComboBox
    Friend WithEvents BtnComPrt As Button
End Class
