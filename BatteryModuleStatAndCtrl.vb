﻿Imports System
Imports System.IO.Ports
Imports System.Runtime.InteropServices      'required to allow the keep system awake while running
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Text



Public Class FrmMain
    Dim ComSerialPort As New SerialPort                                             'Create a serial port
    Dim TxPacketArray() As Byte = {&H52, &H44, &H0, &H0, &H0, &H0, &H0, &H0, &HA}   'Initialise Array of Data to be Sent
    Dim TxPacketSending As Boolean = False
    Dim Logging As Boolean = False
    Dim Pause As Boolean = False
    Dim Mute As Boolean = False
    Dim AutoPause As Boolean = False
    Dim VoltDiffinput As Single
    Dim Path As String
    Dim file As IO.StreamWriter
    Private TxtCBArray As TextBox()
    Private rdoBalArray As RadioButton()
    Private rdoMaxCArray As RadioButton()
    Private rdoMaxStatArray As RadioButton()


    ' Code to keep system awake when programme is running
    <FlagsAttribute()>
    Public Enum EXECUTION_STATE As UInteger
        ES_SYSTEM_REQUIRED = &H1
        ES_DISPLAY_REQUIRED = &H2
        ES_CONTINUOUS = &H80000000UI
    End Enum

    <DllImport("Kernel32.DLL", CharSet:=CharSet.Auto, SetLastError:=True)>
    Private Shared Function SetThreadExecutionState(ByVal state As EXECUTION_STATE) As EXECUTION_STATE
    End Function


    Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles Me.Load

        '0.0.1 Version initial version
        '0.0.2 Added Checksum boxes and error counter
        '0.1.0 Added logging function and created a display and write subroutines
        Dim SW_version As String
        Dim FormName As String
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        SW_version = "V0.1.0"
        FormName = "Battery Module Status and Control - " & SW_version
        Me.Text = FormName  'Display Software name and versio in title bar

        SetThreadExecutionState(EXECUTION_STATE.ES_SYSTEM_REQUIRED) 'keeps system awake but allows displays to go off.


        'populate CmbSerialPort drop down with a list of available com ports.  List defaults to first on list
        CmbSerialPorts.Items.Clear()
        ' Show all available COM ports.
        For Each sp As String In My.Computer.Ports.SerialPortNames
            CmbSerialPorts.Items.Add(sp)        'display all available serial ports in the CmbSerialPorts drop down menu
        Next
        CmbSerialPorts.Text = CmbSerialPorts.Items.Item(1)
        CmbBaudRate.Text = "115200"             'default baud rate is 115200
        VoltDiffinput = TxtVoltDiff.Text
        txtReqRate.Text = (1 / (TmrSendData.Interval * 0.001))
        TxtCBArray = {TxtCB1, TxtCB2, TxtCB3, TxtCB4, TxtCB5, TxtCB6, TxtCB7, TxtCB8, TxtCB9, TxtCB10, TxtCB11, TxtCB12, TxtCB13, TxtCB14, TxtCB15, TxtCB16}
        rdoBalArray = {rdoBalCB1, rdoBalCB2, rdoBalCB3, rdoBalCB4, rdoBalCB5, rdoBalCB6, rdoBalCB7, rdoBalCB8, rdoBalCB9, rdoBalCB10, rdoBalCB11, rdoBalCB12, rdoBalCB13, rdoBalCB14, rdoBalCB15, rdoBalCB16}
        rdoMaxCArray = {rdoMaxC1, rdoMaxC2, rdoMaxC3, rdoMaxC4, rdoMaxC5, rdoMaxC6, rdoMaxC7, rdoMaxC8, rdoMaxC9, rdoMaxC10, rdoMaxC11, rdoMaxC12, rdoMaxC13, rdoMaxC14, rdoMaxC15, rdoMaxC16}
        rdoMaxStatArray = {rdoMaxStat1, rdoMaxStat2, rdoMaxStat3, rdoMaxStat4, rdoMaxStat5, rdoMaxStat6, rdoMaxStat7, rdoMaxStat8}
    End Sub


    Private Sub BtnComPrt_Click(sender As Object, e As EventArgs) Handles BtnComPrt.Click

        Dim ComPort As String
        Dim Baudrate As Integer
        If ComSerialPort.IsOpen Then

            BtnComPrt.Text = "Open Port"
            TxPacketSending = False
            CmdGetData.Text = "Gate Data"
            TmrSendData.Enabled = False
            TmrRxData.Enabled = False
            ComSerialPort.Close()
            Return
        End If

        If CmbSerialPorts.Text IsNot "" Then
            ComPort = CmbSerialPorts.Text
            ComSerialPort.PortName = ComPort
            CmbSerialPorts.BackColor = Color.White
        Else
            CmbSerialPorts.BackColor = Color.Red
            Return
        End If
        If CmbBaudRate.Text IsNot "" Then
            Baudrate = CmbBaudRate.Text
            ComSerialPort.BaudRate = Baudrate
            CmbBaudRate.BackColor = Color.White

        Else
            CmbBaudRate.BackColor = Color.Red
            Return
        End If
        ComSerialPort.Parity = Parity.None
        ComSerialPort.DataBits = 8
        ComSerialPort.StopBits = StopBits.One
        If BtnComPrt.Text Is "Open Port" Then
            ComSerialPort.Open()
            BtnComPrt.Text = "Close Port"

        End If



    End Sub



    Private Sub FrmMain_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ComSerialPort.Close()
    End Sub

    Private Sub CmdGetData_Click(sender As Object, e As EventArgs) Handles CmdGetData.Click
        If ComSerialPort.IsOpen Then

            If TxPacketSending = False Then
                TxPacketSending = True
                CmdGetData.Text = "Stop Tx"
                TmrSendData.Enabled = True
                TmrRxData.Enabled = True
            ElseIf TxPacketSending = True Then
                TxPacketSending = False
                CmdGetData.Text = "Get Data"
                TmrSendData.Enabled = False
                TmrRxData.Enabled = False
            End If
        Else
            MsgBox("Please open COM port")
        End If

    End Sub

    Private Sub TmrSendData_Tick(sender As Object, e As EventArgs) Handles TmrSendData.Tick
        'every time timer interrupts Tx packet generated and sent
        GenerateTxPacket()
        ComSerialPort.Write(TxPacketArray, 0, 9)
    End Sub

    Private Sub GenerateTxPacket()
        Dim checksum As Integer = 0
        'TxPacket Format
        'TxPacketArray(0) -> &H52 Header
        'TxPacketArray(1) -> &H44 Header
        'TxPacketArray(2) -> &H00 Adress of Battery Module Controller
        'TxPacketArray(3) -> &Hxx MSB of Voltage battery difference 1bit = 62.5uV
        'TxPacketArray(4) -> &Hxx LSB of Voltage battery difference 1bit = 62.5uV
        'TxPacketArray(5) -> &Hxx Command Bits
        'TxPacketArray(6) -> &Hxx Spare
        'TxPacketArray(7) -> &Hxx Checksum - modulo 256
        'TxPacketArray(8) -> &H0A Header

        Dim VoltDiffWord As UInt16 = Convert.ToUInt16(VoltDiffinput / 0.0000625)
        Dim VoltDiff() As Byte = BitConverter.GetBytes(VoltDiffWord)
        TxPacketArray(3) = VoltDiff(1)
        TxPacketArray(4) = VoltDiff(0)
        If chkAllowBalance.Checked Then
            If (TxPacketArray(5) And &B1) = 0 Then      'bit not set, so set
                TxPacketArray(5) = TxPacketArray(5) Xor &B1
            End If
        Else
            TxPacketArray(5) = TxPacketArray(5) And &B11111110
        End If

        If chkAllowCharge.Checked Then
            If (TxPacketArray(5) And &B10) = 0 Then      'bit not set, so set
                TxPacketArray(5) = TxPacketArray(5) Xor &B10
            End If
        Else
            TxPacketArray(5) = TxPacketArray(5) And &B11111101
        End If

        'Generate Checksum
        For i As Integer = 3 To 6 Step 1
            checksum = (checksum + TxPacketArray(i)) Mod 256
        Next
        checksum = 256 - checksum
        TxPacketArray(7) = checksum


    End Sub

    Private Sub GetRxPacket()
        Dim RxPacket(255) As Byte
        Dim CellVoltage(16) As Single
        Dim PackVoltage As Single
        Dim MinCellVoltage As Single
        Dim MaxCellVoltage As Single
        Dim TargetVoltage As Single
        Dim Volt_Balance_Diff As Single
        Dim CellVoltageSpread As Single
        Dim ErrCounter As Int16
        Dim BattState As Byte
        Dim Counter As UInt64 = 0
        Dim checksum As Integer = 0
        Dim RXchecksum As Integer = 0
        Dim mask As Integer = 0
        Dim Balance_Status As UInt16
        Dim RxPacketOK As Boolean = False
        'Dim Balance_Test As UInt16
        'Dim BalanceFlag As Boolean
        Dim MaxCStat As UInt16
        'Dim MaxCTest As UInt16
        'Dim MaxCFlag As Boolean
        Dim MaxStat As Byte
        'Dim MaxStatTest As Byte
        'Dim MaxStatFlag As Boolean
        'Dim NeedToSee As Boolean




        If ComSerialPort.BytesToRead >= 91 Then
            Do While ComSerialPort.BytesToRead > 0
                ComSerialPort.Read(RxPacket, 0, ComSerialPort.BytesToRead)
            Loop
            'Later bytes done first to allow color of cell to be changed if CB > target voltage
            'Get Pack voltage and fill

            If RxPacket(0) = &H42 Then          'check pre-amble 
                If RxPacket(1) = &H41 Then      'check pre-amble 
                    If RxPacket(91) = &H44 Then      'check post-amble 
                        If RxPacket(90) = &H4E Then      'check post-amble 
                            For i As Integer = 3 To 87 Step 1 'calculate checksum
                                checksum = (checksum + RxPacket(i)) Mod 256
                            Next
                            checksum = 256 - checksum
                            RXchecksum = RxPacket(88)
                            If checksum <> RXchecksum Then
                                RxPacketOK = False
                            Else
                                RxPacketOK = True
                            End If

                            PackVoltage = RxPacket(40)
                            PackVoltage = PackVoltage << 8 'shift left to fill MSB and allow LSB to be written
                            PackVoltage = (PackVoltage + RxPacket(41)) * 0.001


                            'Get Minimum Cell voltage for balance and fill
                            MinCellVoltage = RxPacket(76)
                            MinCellVoltage = MinCellVoltage << 8  'shift left to fill MSB and allow LSB to be written
                            MinCellVoltage = (MinCellVoltage + RxPacket(77)) * 0.0000625

                            If (MinCellVoltage <= 2.1) Then     'sound and flashing will still sound if and update txt box
                                txtMinCellV.Text = Format(MinCellVoltage, "#.0000")
                                txtMinCellV.BackColor = Color.Red
                                If Mute = False Then

                                    TmrSafetyBing.Enabled = True
                                    Media.SystemSounds.Beep.Play()
                                End If

                            Else
                                txtMinCellV.BackColor = Color.White
                                TmrSafetyBing.Enabled = False
                            End If


                            'Get aximum Cell voltage and fill
                            MaxCellVoltage = RxPacket(78)
                            MaxCellVoltage = MaxCellVoltage << 8  'shift left to fill MSB and allow LSB to be written
                            MaxCellVoltage = (MaxCellVoltage + RxPacket(79)) * 0.0000625

                            If (MaxCellVoltage >= 3.65) Then            'sound and flashing will still sound if and update txt box
                                txtMaxCellV.BackColor = Color.Red
                                txtMinCellV.Text = Format(MinCellVoltage, "#.0000")
                                If Mute = False Then

                                    TmrSafetyBing.Enabled = True
                                    Media.SystemSounds.Beep.Play()
                                End If
                            Else
                                txtMaxCellV.BackColor = Color.White
                                TmrSafetyBing.Enabled = False

                            End If

                            'Read back Voltage Diff target and fill
                            Volt_Balance_Diff = RxPacket(80)
                            Volt_Balance_Diff = Volt_Balance_Diff << 8 'shift left to fill MSB and allow LSB to be written
                            Volt_Balance_Diff = (Volt_Balance_Diff + RxPacket(81)) * 0.0000625

                            'Calculate and display Target Voltage
                            'txtMinCellV.Text = Format((TargetVoltage - Volt_Balance_Diff), "#.0000")
                            TargetVoltage = MinCellVoltage + Volt_Balance_Diff


                            'Read and poplate error counter
                            ErrCounter = RxPacket(82)



                            BattState = RxPacket(83)
                            If chkAllowCharge.Checked = True Then
                                rdoCharge.Enabled = True
                                rdoChargePause.Enabled = True
                                rdoDischarge.Enabled = True

                                Select Case BattState
                                    Case &H0        'battery discharging
                                        rdoCharge.Checked = False
                                        rdoChargePause.Checked = False
                                        rdoDischarge.Checked = True

                                    Case &H55       'battery paused
                                        rdoCharge.Checked = False
                                        rdoChargePause.Checked = True
                                        rdoDischarge.Checked = False

                                    Case &HFF       'battery charging
                                        rdoCharge.Checked = True
                                        rdoChargePause.Checked = False
                                        rdoDischarge.Checked = False

                                    Case Else
                                        rdoCharge.Checked = False
                                        rdoChargePause.Checked = False
                                        rdoDischarge.Checked = False

                                End Select
                            Else
                                rdoCharge.Enabled = False
                                rdoChargePause.Enabled = False
                                rdoDischarge.Enabled = False
                                rdoCharge.Checked = False
                                rdoChargePause.Checked = False
                                rdoDischarge.Checked = False
                            End If


                            'Populate the balancing radio buttons with the current balance staus of the system
                            Balance_Status = RxPacket(3)
                            Balance_Status = Balance_Status << 8    'shift left to fill MSB and allow LSB to be written
                            Balance_Status = Balance_Status + RxPacket(4)   'fill LSB


                            'Populate the C status bits radio buttons
                            MaxStat = RxPacket(5)

                            ' populate the MAX 14921 status bits read from syste,
                            MaxCStat = RxPacket(6)
                            MaxCStat = MaxCStat << 8
                            MaxCStat = MaxCStat + RxPacket(7)

                            'Decode and fill all Cell Voltages
                            For i As Integer = 0 To 15 Step 1
                                CellVoltage(i) = (RxPacket((i * 2) + 8))
                                CellVoltage(i) = CellVoltage(i) << 8
                                CellVoltage(i) = CellVoltage(i) + RxPacket((i * 2) + 9)
                                CellVoltage(i) = CellVoltage(i) * 0.0000625

                            Next


                            'Decode and fill counter value
                            For i As Integer = 0 To 2 Step 1
                                Counter = Counter + RxPacket(i + 84)
                                Counter = Counter << 8
                            Next
                            Counter = Counter + RxPacket(87)

                            CellVoltageSpread = MaxCellVoltage - MinCellVoltage

                        End If
                    End If
                End If
            End If

            If RxPacketOK = True Then
                UpdateWindowValues(PackVoltage, MinCellVoltage, MaxCellVoltage, TargetVoltage, Volt_Balance_Diff, ErrCounter, Counter, checksum, Balance_Status, MaxCStat, MaxStat, RXchecksum, CellVoltageSpread, CellVoltage)
                If Logging = True Then
                    WriteToFile(PackVoltage, MinCellVoltage, MaxCellVoltage, TargetVoltage, Volt_Balance_Diff, Counter, Balance_Status, CellVoltageSpread, CellVoltage)
                End If
            End If


        End If


    End Sub



    Private Sub TxtVoltDiff_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TxtVoltDiff.KeyPress
        Dim temp As Single
        'only if enter or return pressed does an action get taken
        If (e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return)) Or (e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter)) Then
            e.Handled = True
            temp = TxtVoltDiff.Text
            If ((temp > 0.0) And (temp <= 4.095)) Then      'if entered text outside range then revert to last value

                VoltDiffinput = temp
                TxtVoltDiff.Text = Format(VoltDiffinput, "0.000")
            Else
                TxtVoltDiff.Text = Format(VoltDiffinput, "0.000")
            End If

            TxtVoltDiff.SelectionStart = 0
            TxtVoltDiff.SelectAll()
            e.Handled = False
        End If

    End Sub

    Private Sub TxtVoltDiff_Enter(sender As Object, e As EventArgs) Handles TxtVoltDiff.Enter
        TxtVoltDiff.SelectAll()
    End Sub

    Private Sub TxtVoltDiff_Click(sender As Object, e As EventArgs) Handles TxtVoltDiff.Click
        TxtVoltDiff.SelectAll()
    End Sub

    Private Sub TxtReqRate_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtReqRate.KeyPress
        Dim temp As Integer
        'only if enter or return pressed does an action get taken
        If (e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return)) Or (e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Enter)) Then
            e.Handled = True
            temp = txtReqRate.Text
            If (temp >= 0.1) And (temp <= 100) Then
                TmrSendData.Interval = (1 / temp) * 1000
            Else
                txtReqRate.Text = (1 / (TmrSendData.Interval * 0.001))

            End If
            txtReqRate.SelectionStart = 0
            txtReqRate.SelectAll()
        End If


    End Sub
    Private Sub TxtReqRate_Click(sender As Object, e As EventArgs) Handles txtReqRate.Click
        'if box clicked then all in cell selected
        txtReqRate.SelectAll()
    End Sub

    Private Sub TmrRxData_Tick(sender As Object, e As EventArgs) Handles TmrRxData.Tick
        'Every 1ms Rx serial port buffer checked for data
        GetRxPacket()
    End Sub

    Private Sub BtnPause_Click(sender As Object, e As EventArgs) Handles btnPause.Click
        If Pause = True Then
            btnPause.Text = "Pause"
            btnPause.BackColor = Color.LightGray
            Pause = False
        Else
            Pause = True
            btnPause.Text = "Resume"
            btnPause.BackColor = Color.Red
        End If
    End Sub

    Private Sub BtnAutoPause_Click(sender As Object, e As EventArgs) Handles btnAutoPause.Click
        If AutoPause = True Then
            btnAutoPause.Text = "Auto Pause Off"
            btnAutoPause.BackColor = Color.LightGray

            AutoPause = False
        Else
            AutoPause = True
            btnAutoPause.Text = "Auto Pause On"
            btnAutoPause.BackColor = Color.Green
        End If
    End Sub

    Private Sub TmrSafetyBing_Tick(sender As Object, e As EventArgs) Handles TmrSafetyBing.Tick
        Media.SystemSounds.Beep.Play()
    End Sub

    Private Sub BtnMute_Click(sender As Object, e As EventArgs) Handles BtnMute.Click
        If Mute = True Then
            BtnMute.Text = "Mute"
            BtnMute.BackColor = Color.LightGray

            Mute = False
        Else
            Mute = True
            BtnMute.Text = "Mute On"
            BtnMute.BackColor = Color.Red
        End If
    End Sub



    Private Sub BtnSaveFile_Click(sender As Object, e As EventArgs) Handles BtnSaveFile.Click
        BtnSaveFile.BackColor = Color.LightGray
        SaveLogFileDlg.Filter = "CSV File|*.csv"
        SaveLogFileDlg.Title = "Save a log file"
        'SaveLogFileDlg.DefaultExt = "*.csv"
        SaveLogFileDlg.FileName = "Battery Balance Log File_" & String.Format("{0:yyyy-MM-dd}", DateTime.Now) & "_" & String.Format("{0:HHmmss}", DateTime.Now)
        Dim result As DialogResult = SaveLogFileDlg.ShowDialog()
        ' Test result.
        If result = Windows.Forms.DialogResult.OK Then

            ' Get the file name.


            If SaveLogFileDlg.FileName <> "" Then
                Path = SaveLogFileDlg.FileName
            End If



        Else
            Path = ""       'user does not want to overwrite file so path is cleared if logging button clicked it will return not work
        End If
    End Sub

    Private Sub BtnLog_Click(sender As Object, e As EventArgs) Handles BtnLog.Click

        Dim headerstring As String
        If Logging = True Then
            BtnLog.Text = "Logging On"
            BtnLog.BackColor = Color.LightGray
            BtnLog.Font = New Font(BtnLog.Font, FontStyle.Regular)
            Logging = False

        Else
            If Path <> "" Then      'make sure that there is a path and filename defined
                If My.Computer.FileSystem.FileExists(Path) Then     'Check to see if file is already there
                    Dim result2 As DialogResult = MsgBox("File Aready Exists, OVERWRITE?", MsgBoxStyle.OkCancel)
                    If result2 = Windows.Forms.DialogResult.OK Then
                        My.Computer.FileSystem.DeleteFile(Path)         'Delete old file if it is there.  Need to add more checks here and get user responce from SaveLogFileDlg overwrite dialog
                    Else
                        Path = ""
                        Return
                    End If
                End If
                Logging = True
                BtnLog.Text = "Logging Off"
                BtnLog.BackColor = Color.LightGreen                 'show logging is on
                BtnLog.Font = New Font(BtnLog.Font, FontStyle.Bold)
                'header string: Date, Time,Counter,Cell 1, Bal 1, ..., Cell 16, Bal 16, Cell Min, Cell Max, Volt Diff, Target V"
                file = My.Computer.FileSystem.OpenTextFileWriter(Path, True)    'set up file variable to open file for writing
                headerstring = "Date,Time,Counter,"                                 'headerstring is the first line of CSV file
                For i As Integer = 1 To 16 Step 1
                    headerstring = headerstring & "Cell" & i & " V,Bal" & i & ","
                Next
                headerstring = headerstring & "Cell Min,Cell Max,Pack Voltage,Volt Diff,Target V,CellVSpread"
                file.WriteLine(headerstring)
                headerstring = "yyyyMMdd,HH:MM:SS.fff,ms,"
                For i As Integer = 1 To 16 Step 1
                    headerstring = headerstring & "V,Boolean,"
                Next
                headerstring = headerstring & "V,V,Pack V,V Low,V,V Low"
                file.WriteLine(headerstring)
                file.Close()


            Else                            'reset button if there is no file and path defined
                BtnLog.Text = "Logging On"
                BtnLog.BackColor = Color.LightGray
                BtnLog.Font = New Font(BtnLog.Font, FontStyle.Regular)
                Logging = False
                BtnSaveFile.BackColor = Color.Red       'highlight file button
                MsgBox("No File Name Selected or overwrite not accepted")         '... and display message

            End If

        End If
        Return
    End Sub

    Private Sub UpdateWindowValues(ByVal PackVoltage As Single, ByVal MinCellVoltage As Single, ByVal MaxCellVoltage As Single, TargetVoltage As Single, Volt_Balance_Diff As Single, ErrCounter As Int16, Counter As UInt64, checksum As Integer, Balance_Status As UInt16, MaxCStat As UInt16, MaxStat As Byte, RXchecksum As Integer, CellVoltageSpread As Single, ByVal ParamArray CellVoltage() As Single)

        Dim mask As Integer = 0
        Dim MinVTest As Single = 4.096
        Dim MaxVTest As Single = 0
        Dim Balance_Test As UInt16 = 0
        Dim BalanceFlag As Boolean = 0
        Dim MaxCTest As UInt16 = 0
        Dim MaxCFlag As Boolean = 0
        Dim MaxStatTest As Byte = 0
        Dim MaxStatFlag As Boolean = 0
        Dim NeedToSee As Boolean = 0
        Dim MinCellVPointer As Integer = 0
        Dim MaxCellVPointer As Integer = 0

        If Pause = False Then 'update all text boxes if pause not selected

            If CellVoltage(1) <= 3 Then
                Dim test As Integer = 100
            End If
            txtChkSumCalc.Text = Convert.ToString(checksum)
            txtChkSumRead.Text = Convert.ToString(RXchecksum)
            txtPackV.Text = Format(PackVoltage, "#.00")
            txtMinCellV.Text = Format(MinCellVoltage, "#.0000")
            txtMaxCellV.Text = Format(MaxCellVoltage, "#.0000")
            txtReadVoltDiff.Text = Format(Volt_Balance_Diff, "0.00")
            txtTargetV.Text = Format((TargetVoltage), "#.0000")
            txtCellVSpread.Text = Format((CellVoltageSpread), "0.000")
            txtErrCounter.Text = ErrCounter
            For i As Integer = 0 To 15 Step 1
                mask = 1 << i   'create mask to AND with status
                Balance_Test = Balance_Status And mask
                Balance_Test = Balance_Test >> i
                BalanceFlag = Balance_Test
                If BalanceFlag = True Then
                    rdoBalArray(i).Checked = True
                Else
                    rdoBalArray(i).Checked = False
                End If
            Next
            For i As Integer = 0 To 7 Step 1
                mask = 1 << i
                MaxStatTest = MaxStat And mask
                MaxStatTest = MaxStatTest >> i
                MaxStatFlag = MaxStatTest
                If MaxStatFlag = True Then
                    If i <> 2 Then      'bit 2 is always set showing rev0 so ignore this bit
                        NeedToSee = True
                    End If
                    rdoMaxStatArray(i).Checked = True

                Else
                    rdoMaxStatArray(i).Checked = False
                End If
            Next
            For i As Integer = 0 To 15 Step 1
                mask = 1 << i
                MaxCTest = MaxCStat And mask
                MaxCTest = MaxCTest >> i
                MaxCFlag = MaxCTest
                If MaxCFlag = True Then
                    rdoMaxCArray(i).Checked = True
                    NeedToSee = True
                Else
                    rdoMaxCArray(i).Checked = False
                End If
            Next
            For i As Integer = 0 To 15 Step 1
                TxtCBArray(i).Font = New Font(TxtCBArray(i).Font, FontStyle.Regular)
                TxtCBArray(i).ForeColor = Color.Black
                TxtCBArray(i).Text = Format(CellVoltage(i), "#.0000")
                If CellVoltage(i) > TargetVoltage Then
                    TxtCBArray(i).BackColor = Color.Red
                    NeedToSee = True
                Else
                    TxtCBArray(i).BackColor = Color.White
                End If
                If CellVoltage(i) < MinVTest Then
                    MinVTest = CellVoltage(i)
                    MinCellVPointer = i
                End If
                If CellVoltage(i) > MaxVTest Then
                    MaxVTest = CellVoltage(i)
                    MaxCellVPointer = i
                End If
            Next

            txtCounter.Text = Convert.ToString(Counter)
            TxtCBArray(MinCellVPointer).Font = New Font(TxtCBArray(MinCellVPointer).Font, FontStyle.Bold)
            TxtCBArray(MinCellVPointer).ForeColor = Color.DarkTurquoise
            TxtCBArray(MaxCellVPointer).Font = New Font(TxtCBArray(MaxCellVPointer).Font, FontStyle.Bold)
            TxtCBArray(MaxCellVPointer).ForeColor = Color.YellowGreen
            If CellVoltage(MaxCellVPointer) > TargetVoltage Then
                TxtCBArray(MaxCellVPointer).BackColor = Color.DeepPink
                NeedToSee = True
            End If

        End If


        If AutoPause = True Then
            If NeedToSee = True Then
                NeedToSee = False
                Pause = True
                btnPause.Text = "Resume"
                btnPause.BackColor = Color.Red
            End If
        End If
        Return
    End Sub

    Private Sub WriteToFile(ByVal PackVoltage As Single, ByVal MinCellVoltage As Single, ByVal MaxCellVoltage As Single, TargetVoltage As Single, Volt_Balance_Diff As Single, Counter As UInt64, Balance_Status As UInt16, CellVoltageSpread As Single, ByVal ParamArray CellVoltage() As Single)
        Dim mask As Integer = 0
        Dim Balance_Test As UInt16 = 0
        Dim BalArray(16) As Integer
        Dim WriteString As String
        'header string: Date, Time,Counter,Cell 1, Bal 1, ..., Cell 16, Bal 16, Cell Min, Cell Max, Pack Voltage, Volt Diff, Target V"
        file = My.Computer.FileSystem.OpenTextFileWriter(Path, True)    'set up file variable to open file for writing
        WriteString = String.Format("{0:yyyyMMdd}", DateTime.Now) & "," & String.Format("{0:HH:mm:ss.fff}", DateTime.Now) & "," & Convert.ToString(Counter) & ","

        For i As Integer = 0 To 15 Step 1
            mask = 1 << i   'create mask to AND with status
            Balance_Test = Balance_Status And mask
            Balance_Test = Balance_Test >> i
            BalArray(i) = Balance_Test
        Next
        For i As Integer = 0 To 15 Step 1
            WriteString = WriteString & Convert.ToString(Format(CellVoltage(i), "#.0000")) & "," & Convert.ToString(BalArray(i)) & ","
        Next
        WriteString = WriteString & Convert.ToString(Format(MinCellVoltage, "#.0000")) & "," & Convert.ToString(Format(MaxCellVoltage, "#.0000"))
        WriteString = WriteString & "," & Convert.ToString(Format(PackVoltage, "#.00")) & "," & Convert.ToString(Format(Volt_Balance_Diff, "0.00"))
        WriteString = WriteString & "," & Convert.ToString(Format(TargetVoltage, "#.0000")) & "," & Convert.ToString(Format(CellVoltageSpread, "0.000"))
        file.WriteLine(WriteString)
        file.Close()
        Return
    End Sub


End Class




