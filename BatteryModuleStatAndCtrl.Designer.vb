﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CmbSerialPorts = New System.Windows.Forms.ComboBox()
        Me.TxtSerialPrt = New System.Windows.Forms.TextBox()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.TxtBaudRate = New System.Windows.Forms.TextBox()
        Me.CmbBaudRate = New System.Windows.Forms.ComboBox()
        Me.BtnComPrt = New System.Windows.Forms.Button()
        Me.CmdGetData = New System.Windows.Forms.Button()
        Me.TxtCB1 = New System.Windows.Forms.TextBox()
        Me.TxtCB2 = New System.Windows.Forms.TextBox()
        Me.TxtCB3 = New System.Windows.Forms.TextBox()
        Me.TxtCB4 = New System.Windows.Forms.TextBox()
        Me.TxtCB5 = New System.Windows.Forms.TextBox()
        Me.TxtCB10 = New System.Windows.Forms.TextBox()
        Me.TxtCB9 = New System.Windows.Forms.TextBox()
        Me.TxtCB8 = New System.Windows.Forms.TextBox()
        Me.TxtCB7 = New System.Windows.Forms.TextBox()
        Me.TxtCB6 = New System.Windows.Forms.TextBox()
        Me.TxtCB15 = New System.Windows.Forms.TextBox()
        Me.TxtCB14 = New System.Windows.Forms.TextBox()
        Me.TxtCB13 = New System.Windows.Forms.TextBox()
        Me.TxtCB12 = New System.Windows.Forms.TextBox()
        Me.TxtCB11 = New System.Windows.Forms.TextBox()
        Me.TxtCB16 = New System.Windows.Forms.TextBox()
        Me.TmrSendData = New System.Windows.Forms.Timer(Me.components)
        Me.LblCB1 = New System.Windows.Forms.Label()
        Me.LblCB2 = New System.Windows.Forms.Label()
        Me.LblCB4 = New System.Windows.Forms.Label()
        Me.LblCB3 = New System.Windows.Forms.Label()
        Me.LblCB8 = New System.Windows.Forms.Label()
        Me.LblCB7 = New System.Windows.Forms.Label()
        Me.LblCB6 = New System.Windows.Forms.Label()
        Me.LblCB5 = New System.Windows.Forms.Label()
        Me.LblCB12 = New System.Windows.Forms.Label()
        Me.LblCB11 = New System.Windows.Forms.Label()
        Me.LblCB10 = New System.Windows.Forms.Label()
        Me.LblCB9 = New System.Windows.Forms.Label()
        Me.LblCB16 = New System.Windows.Forms.Label()
        Me.LblCB15 = New System.Windows.Forms.Label()
        Me.LblCB14 = New System.Windows.Forms.Label()
        Me.LblCB13 = New System.Windows.Forms.Label()
        Me.LblRxData = New System.Windows.Forms.Label()
        Me.lblTxData = New System.Windows.Forms.Label()
        Me.lblAllowBalce = New System.Windows.Forms.Label()
        Me.lblVoltDiff = New System.Windows.Forms.Label()
        Me.TxtVoltDiff = New System.Windows.Forms.TextBox()
        Me.chkAllowBalance = New System.Windows.Forms.CheckBox()
        Me.lblTargetV = New System.Windows.Forms.Label()
        Me.txtTargetV = New System.Windows.Forms.TextBox()
        Me.lblPackV = New System.Windows.Forms.Label()
        Me.txtPackV = New System.Windows.Forms.TextBox()
        Me.lblCellV = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rdoBalCB1 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB2 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB3 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB4 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB8 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB7 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB6 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB5 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB12 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB11 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB10 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB9 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB16 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB15 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB14 = New System.Windows.Forms.RadioButton()
        Me.rdoBalCB13 = New System.Windows.Forms.RadioButton()
        Me.txtReqRate = New System.Windows.Forms.TextBox()
        Me.lblRequestRate = New System.Windows.Forms.Label()
        Me.lblHz = New System.Windows.Forms.Label()
        Me.TmrRxData = New System.Windows.Forms.Timer(Me.components)
        Me.lblCounter = New System.Windows.Forms.Label()
        Me.txtCounter = New System.Windows.Forms.TextBox()
        Me.lblReadVoltDelta = New System.Windows.Forms.Label()
        Me.txtReadVoltDiff = New System.Windows.Forms.TextBox()
        Me.lblMinCellV = New System.Windows.Forms.Label()
        Me.txtMinCellV = New System.Windows.Forms.TextBox()
        Me.lblMaxC1 = New System.Windows.Forms.Label()
        Me.lblMaxC2 = New System.Windows.Forms.Label()
        Me.lblMaxC3 = New System.Windows.Forms.Label()
        Me.lblMaxC4 = New System.Windows.Forms.Label()
        Me.lblMaxC5 = New System.Windows.Forms.Label()
        Me.lblMaxC6 = New System.Windows.Forms.Label()
        Me.lblMaxC7 = New System.Windows.Forms.Label()
        Me.lblMaxC8 = New System.Windows.Forms.Label()
        Me.lblMaxC16 = New System.Windows.Forms.Label()
        Me.lblMaxC15 = New System.Windows.Forms.Label()
        Me.lblMaxC14 = New System.Windows.Forms.Label()
        Me.lblMaxC13 = New System.Windows.Forms.Label()
        Me.lblMaxC12 = New System.Windows.Forms.Label()
        Me.lblMaxC11 = New System.Windows.Forms.Label()
        Me.lblMaxC10 = New System.Windows.Forms.Label()
        Me.lblMaxC9 = New System.Windows.Forms.Label()
        Me.rdoMaxC1 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC2 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC3 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC4 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC5 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC6 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC7 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC8 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC9 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC10 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC11 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC12 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC13 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC14 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC15 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxC16 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat8 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat7 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat6 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat5 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat4 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat3 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat2 = New System.Windows.Forms.RadioButton()
        Me.rdoMaxStat1 = New System.Windows.Forms.RadioButton()
        Me.lblMaxStat8 = New System.Windows.Forms.Label()
        Me.lblMaxStat7 = New System.Windows.Forms.Label()
        Me.lblMaxStat6 = New System.Windows.Forms.Label()
        Me.lblMaxStat5 = New System.Windows.Forms.Label()
        Me.lblMaxStat4 = New System.Windows.Forms.Label()
        Me.lblMaxStat3 = New System.Windows.Forms.Label()
        Me.lblMaxStat2 = New System.Windows.Forms.Label()
        Me.lblMaxStat1 = New System.Windows.Forms.Label()
        Me.btnPause = New System.Windows.Forms.Button()
        Me.btnAutoPause = New System.Windows.Forms.Button()
        Me.Tip = New System.Windows.Forms.ToolTip(Me.components)
        Me.BtnMute = New System.Windows.Forms.Button()
        Me.pctLevelEnergy = New System.Windows.Forms.PictureBox()
        Me.lblErrCounter = New System.Windows.Forms.Label()
        Me.txtErrCounter = New System.Windows.Forms.TextBox()
        Me.txtChkSumCalc = New System.Windows.Forms.TextBox()
        Me.txtChkSumRead = New System.Windows.Forms.TextBox()
        Me.lblChkSumRead = New System.Windows.Forms.Label()
        Me.lblChkSumCalc = New System.Windows.Forms.Label()
        Me.LblDebug = New System.Windows.Forms.Label()
        Me.lblSerialPort = New System.Windows.Forms.Label()
        Me.lblMaxCellV = New System.Windows.Forms.Label()
        Me.txtMaxCellV = New System.Windows.Forms.TextBox()
        Me.chkAllowCharge = New System.Windows.Forms.CheckBox()
        Me.lblAllowCharge = New System.Windows.Forms.Label()
        Me.TmrSafetyBing = New System.Windows.Forms.Timer(Me.components)
        Me.rdoCharge = New System.Windows.Forms.RadioButton()
        Me.lblCharge = New System.Windows.Forms.Label()
        Me.rdoDischarge = New System.Windows.Forms.RadioButton()
        Me.lblDisCharge = New System.Windows.Forms.Label()
        Me.rdoChargePause = New System.Windows.Forms.RadioButton()
        Me.lblChargePause = New System.Windows.Forms.Label()
        Me.SaveLogFileDlg = New System.Windows.Forms.SaveFileDialog()
        Me.BtnSaveFile = New System.Windows.Forms.Button()
        Me.BtnLog = New System.Windows.Forms.Button()
        Me.lblCellVSpread = New System.Windows.Forms.Label()
        Me.txtCellVSpread = New System.Windows.Forms.TextBox()
        CType(Me.pctLevelEnergy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'CmbSerialPorts
        '
        Me.CmbSerialPorts.BackColor = System.Drawing.Color.White
        Me.CmbSerialPorts.ForeColor = System.Drawing.SystemColors.WindowText
        Me.CmbSerialPorts.FormattingEnabled = True
        Me.CmbSerialPorts.Location = New System.Drawing.Point(672, 313)
        Me.CmbSerialPorts.Name = "CmbSerialPorts"
        Me.CmbSerialPorts.Size = New System.Drawing.Size(121, 21)
        Me.CmbSerialPorts.TabIndex = 2
        '
        'TxtSerialPrt
        '
        Me.TxtSerialPrt.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TxtSerialPrt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtSerialPrt.ForeColor = System.Drawing.Color.Black
        Me.TxtSerialPrt.Location = New System.Drawing.Point(672, 296)
        Me.TxtSerialPrt.Name = "TxtSerialPrt"
        Me.TxtSerialPrt.Size = New System.Drawing.Size(100, 13)
        Me.TxtSerialPrt.TabIndex = 3
        Me.TxtSerialPrt.Text = "Select Serial Port"
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(61, 4)
        '
        'TxtBaudRate
        '
        Me.TxtBaudRate.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TxtBaudRate.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TxtBaudRate.ForeColor = System.Drawing.Color.Black
        Me.TxtBaudRate.Location = New System.Drawing.Point(672, 340)
        Me.TxtBaudRate.Name = "TxtBaudRate"
        Me.TxtBaudRate.Size = New System.Drawing.Size(100, 13)
        Me.TxtBaudRate.TabIndex = 5
        Me.TxtBaudRate.Text = "Select Baud Rate"
        '
        'CmbBaudRate
        '
        Me.CmbBaudRate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.CmbBaudRate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CmbBaudRate.FormattingEnabled = True
        Me.CmbBaudRate.Items.AddRange(New Object() {"9600", "19200", "38400", "57600", "115200", "230400", "312500"})
        Me.CmbBaudRate.Location = New System.Drawing.Point(672, 359)
        Me.CmbBaudRate.Name = "CmbBaudRate"
        Me.CmbBaudRate.Size = New System.Drawing.Size(121, 21)
        Me.CmbBaudRate.TabIndex = 6
        '
        'BtnComPrt
        '
        Me.BtnComPrt.Location = New System.Drawing.Point(672, 386)
        Me.BtnComPrt.Name = "BtnComPrt"
        Me.BtnComPrt.Size = New System.Drawing.Size(75, 23)
        Me.BtnComPrt.TabIndex = 7
        Me.BtnComPrt.Text = "Open Port"
        Me.BtnComPrt.UseVisualStyleBackColor = True
        '
        'CmdGetData
        '
        Me.CmdGetData.Location = New System.Drawing.Point(672, 415)
        Me.CmdGetData.Name = "CmdGetData"
        Me.CmdGetData.Size = New System.Drawing.Size(75, 23)
        Me.CmdGetData.TabIndex = 8
        Me.CmdGetData.Text = "Get Data"
        Me.Tip.SetToolTip(Me.CmdGetData, "Starts asking for data, only works if  port is open")
        Me.CmdGetData.UseVisualStyleBackColor = True
        '
        'TxtCB1
        '
        Me.TxtCB1.Location = New System.Drawing.Point(77, 50)
        Me.TxtCB1.Name = "TxtCB1"
        Me.TxtCB1.ReadOnly = True
        Me.TxtCB1.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB1.TabIndex = 9
        '
        'TxtCB2
        '
        Me.TxtCB2.Location = New System.Drawing.Point(77, 76)
        Me.TxtCB2.Name = "TxtCB2"
        Me.TxtCB2.ReadOnly = True
        Me.TxtCB2.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB2.TabIndex = 10
        '
        'TxtCB3
        '
        Me.TxtCB3.Location = New System.Drawing.Point(77, 102)
        Me.TxtCB3.Name = "TxtCB3"
        Me.TxtCB3.ReadOnly = True
        Me.TxtCB3.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB3.TabIndex = 11
        '
        'TxtCB4
        '
        Me.TxtCB4.Location = New System.Drawing.Point(77, 128)
        Me.TxtCB4.Name = "TxtCB4"
        Me.TxtCB4.ReadOnly = True
        Me.TxtCB4.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB4.TabIndex = 12
        '
        'TxtCB5
        '
        Me.TxtCB5.Location = New System.Drawing.Point(77, 154)
        Me.TxtCB5.Name = "TxtCB5"
        Me.TxtCB5.ReadOnly = True
        Me.TxtCB5.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB5.TabIndex = 13
        '
        'TxtCB10
        '
        Me.TxtCB10.Location = New System.Drawing.Point(77, 284)
        Me.TxtCB10.Name = "TxtCB10"
        Me.TxtCB10.ReadOnly = True
        Me.TxtCB10.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB10.TabIndex = 18
        '
        'TxtCB9
        '
        Me.TxtCB9.Location = New System.Drawing.Point(77, 258)
        Me.TxtCB9.Name = "TxtCB9"
        Me.TxtCB9.ReadOnly = True
        Me.TxtCB9.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB9.TabIndex = 17
        '
        'TxtCB8
        '
        Me.TxtCB8.Location = New System.Drawing.Point(77, 232)
        Me.TxtCB8.Name = "TxtCB8"
        Me.TxtCB8.ReadOnly = True
        Me.TxtCB8.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB8.TabIndex = 16
        '
        'TxtCB7
        '
        Me.TxtCB7.Location = New System.Drawing.Point(77, 206)
        Me.TxtCB7.Name = "TxtCB7"
        Me.TxtCB7.ReadOnly = True
        Me.TxtCB7.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB7.TabIndex = 15
        '
        'TxtCB6
        '
        Me.TxtCB6.Location = New System.Drawing.Point(77, 180)
        Me.TxtCB6.Name = "TxtCB6"
        Me.TxtCB6.ReadOnly = True
        Me.TxtCB6.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB6.TabIndex = 14
        '
        'TxtCB15
        '
        Me.TxtCB15.Location = New System.Drawing.Point(77, 414)
        Me.TxtCB15.Name = "TxtCB15"
        Me.TxtCB15.ReadOnly = True
        Me.TxtCB15.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB15.TabIndex = 23
        '
        'TxtCB14
        '
        Me.TxtCB14.Location = New System.Drawing.Point(77, 388)
        Me.TxtCB14.Name = "TxtCB14"
        Me.TxtCB14.ReadOnly = True
        Me.TxtCB14.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB14.TabIndex = 22
        '
        'TxtCB13
        '
        Me.TxtCB13.Location = New System.Drawing.Point(77, 362)
        Me.TxtCB13.Name = "TxtCB13"
        Me.TxtCB13.ReadOnly = True
        Me.TxtCB13.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB13.TabIndex = 21
        '
        'TxtCB12
        '
        Me.TxtCB12.Location = New System.Drawing.Point(77, 336)
        Me.TxtCB12.Name = "TxtCB12"
        Me.TxtCB12.ReadOnly = True
        Me.TxtCB12.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB12.TabIndex = 20
        '
        'TxtCB11
        '
        Me.TxtCB11.Location = New System.Drawing.Point(77, 310)
        Me.TxtCB11.Name = "TxtCB11"
        Me.TxtCB11.ReadOnly = True
        Me.TxtCB11.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB11.TabIndex = 19
        '
        'TxtCB16
        '
        Me.TxtCB16.Location = New System.Drawing.Point(77, 440)
        Me.TxtCB16.Name = "TxtCB16"
        Me.TxtCB16.ReadOnly = True
        Me.TxtCB16.Size = New System.Drawing.Size(63, 20)
        Me.TxtCB16.TabIndex = 24
        '
        'TmrSendData
        '
        Me.TmrSendData.Interval = 10
        '
        'LblCB1
        '
        Me.LblCB1.AutoSize = True
        Me.LblCB1.Location = New System.Drawing.Point(14, 53)
        Me.LblCB1.Name = "LblCB1"
        Me.LblCB1.Size = New System.Drawing.Size(63, 13)
        Me.LblCB1.TabIndex = 44
        Me.LblCB1.Text = "Battery CB1"
        '
        'LblCB2
        '
        Me.LblCB2.AutoSize = True
        Me.LblCB2.Location = New System.Drawing.Point(14, 79)
        Me.LblCB2.Name = "LblCB2"
        Me.LblCB2.Size = New System.Drawing.Size(63, 13)
        Me.LblCB2.TabIndex = 45
        Me.LblCB2.Text = "Battery CB2"
        '
        'LblCB4
        '
        Me.LblCB4.AutoSize = True
        Me.LblCB4.Location = New System.Drawing.Point(14, 131)
        Me.LblCB4.Name = "LblCB4"
        Me.LblCB4.Size = New System.Drawing.Size(63, 13)
        Me.LblCB4.TabIndex = 47
        Me.LblCB4.Text = "Battery CB4"
        '
        'LblCB3
        '
        Me.LblCB3.AutoSize = True
        Me.LblCB3.Location = New System.Drawing.Point(14, 105)
        Me.LblCB3.Name = "LblCB3"
        Me.LblCB3.Size = New System.Drawing.Size(63, 13)
        Me.LblCB3.TabIndex = 46
        Me.LblCB3.Text = "Battery CB3"
        '
        'LblCB8
        '
        Me.LblCB8.AutoSize = True
        Me.LblCB8.Location = New System.Drawing.Point(14, 235)
        Me.LblCB8.Name = "LblCB8"
        Me.LblCB8.Size = New System.Drawing.Size(63, 13)
        Me.LblCB8.TabIndex = 51
        Me.LblCB8.Text = "Battery CB8"
        '
        'LblCB7
        '
        Me.LblCB7.AutoSize = True
        Me.LblCB7.Location = New System.Drawing.Point(14, 209)
        Me.LblCB7.Name = "LblCB7"
        Me.LblCB7.Size = New System.Drawing.Size(63, 13)
        Me.LblCB7.TabIndex = 50
        Me.LblCB7.Text = "Battery CB7"
        '
        'LblCB6
        '
        Me.LblCB6.AutoSize = True
        Me.LblCB6.Location = New System.Drawing.Point(14, 183)
        Me.LblCB6.Name = "LblCB6"
        Me.LblCB6.Size = New System.Drawing.Size(63, 13)
        Me.LblCB6.TabIndex = 49
        Me.LblCB6.Text = "Battery CB6"
        '
        'LblCB5
        '
        Me.LblCB5.AutoSize = True
        Me.LblCB5.Location = New System.Drawing.Point(14, 157)
        Me.LblCB5.Name = "LblCB5"
        Me.LblCB5.Size = New System.Drawing.Size(63, 13)
        Me.LblCB5.TabIndex = 48
        Me.LblCB5.Text = "Battery CB5"
        '
        'LblCB12
        '
        Me.LblCB12.AutoSize = True
        Me.LblCB12.Location = New System.Drawing.Point(8, 339)
        Me.LblCB12.Name = "LblCB12"
        Me.LblCB12.Size = New System.Drawing.Size(69, 13)
        Me.LblCB12.TabIndex = 55
        Me.LblCB12.Text = "Battery CB12"
        '
        'LblCB11
        '
        Me.LblCB11.AutoSize = True
        Me.LblCB11.Location = New System.Drawing.Point(8, 313)
        Me.LblCB11.Name = "LblCB11"
        Me.LblCB11.Size = New System.Drawing.Size(69, 13)
        Me.LblCB11.TabIndex = 54
        Me.LblCB11.Text = "Battery CB11"
        '
        'LblCB10
        '
        Me.LblCB10.AutoSize = True
        Me.LblCB10.Location = New System.Drawing.Point(8, 287)
        Me.LblCB10.Name = "LblCB10"
        Me.LblCB10.Size = New System.Drawing.Size(69, 13)
        Me.LblCB10.TabIndex = 53
        Me.LblCB10.Text = "Battery CB10"
        '
        'LblCB9
        '
        Me.LblCB9.AutoSize = True
        Me.LblCB9.Location = New System.Drawing.Point(14, 261)
        Me.LblCB9.Name = "LblCB9"
        Me.LblCB9.Size = New System.Drawing.Size(63, 13)
        Me.LblCB9.TabIndex = 52
        Me.LblCB9.Text = "Battery CB9"
        '
        'LblCB16
        '
        Me.LblCB16.AutoSize = True
        Me.LblCB16.Location = New System.Drawing.Point(8, 443)
        Me.LblCB16.Name = "LblCB16"
        Me.LblCB16.Size = New System.Drawing.Size(69, 13)
        Me.LblCB16.TabIndex = 59
        Me.LblCB16.Text = "Battery CB16"
        '
        'LblCB15
        '
        Me.LblCB15.AutoSize = True
        Me.LblCB15.Location = New System.Drawing.Point(8, 417)
        Me.LblCB15.Name = "LblCB15"
        Me.LblCB15.Size = New System.Drawing.Size(69, 13)
        Me.LblCB15.TabIndex = 58
        Me.LblCB15.Text = "Battery CB15"
        '
        'LblCB14
        '
        Me.LblCB14.AutoSize = True
        Me.LblCB14.Location = New System.Drawing.Point(8, 391)
        Me.LblCB14.Name = "LblCB14"
        Me.LblCB14.Size = New System.Drawing.Size(69, 13)
        Me.LblCB14.TabIndex = 57
        Me.LblCB14.Text = "Battery CB14"
        '
        'LblCB13
        '
        Me.LblCB13.AutoSize = True
        Me.LblCB13.Location = New System.Drawing.Point(8, 365)
        Me.LblCB13.Name = "LblCB13"
        Me.LblCB13.Size = New System.Drawing.Size(69, 13)
        Me.LblCB13.TabIndex = 56
        Me.LblCB13.Text = "Battery CB13"
        '
        'LblRxData
        '
        Me.LblRxData.AutoSize = True
        Me.LblRxData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRxData.Location = New System.Drawing.Point(156, 9)
        Me.LblRxData.Name = "LblRxData"
        Me.LblRxData.Size = New System.Drawing.Size(92, 13)
        Me.LblRxData.TabIndex = 60
        Me.LblRxData.Text = "Received Data"
        '
        'lblTxData
        '
        Me.lblTxData.AutoSize = True
        Me.lblTxData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTxData.Location = New System.Drawing.Point(681, 9)
        Me.lblTxData.Name = "lblTxData"
        Me.lblTxData.Size = New System.Drawing.Size(104, 13)
        Me.lblTxData.TabIndex = 61
        Me.lblTxData.Text = "Transmitted Data"
        '
        'lblAllowBalce
        '
        Me.lblAllowBalce.AutoSize = True
        Me.lblAllowBalce.Location = New System.Drawing.Point(665, 32)
        Me.lblAllowBalce.Name = "lblAllowBalce"
        Me.lblAllowBalce.Size = New System.Drawing.Size(82, 13)
        Me.lblAllowBalce.TabIndex = 62
        Me.lblAllowBalce.Text = "Allow Balancing"
        '
        'lblVoltDiff
        '
        Me.lblVoltDiff.AutoSize = True
        Me.lblVoltDiff.Location = New System.Drawing.Point(652, 69)
        Me.lblVoltDiff.Name = "lblVoltDiff"
        Me.lblVoltDiff.Size = New System.Drawing.Size(95, 13)
        Me.lblVoltDiff.TabIndex = 63
        Me.lblVoltDiff.Text = "Voltage Difference"
        '
        'TxtVoltDiff
        '
        Me.TxtVoltDiff.BackColor = System.Drawing.Color.White
        Me.TxtVoltDiff.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtVoltDiff.Location = New System.Drawing.Point(745, 66)
        Me.TxtVoltDiff.Name = "TxtVoltDiff"
        Me.TxtVoltDiff.Size = New System.Drawing.Size(48, 20)
        Me.TxtVoltDiff.TabIndex = 64
        Me.TxtVoltDiff.Text = "0.02"
        '
        'chkAllowBalance
        '
        Me.chkAllowBalance.AutoSize = True
        Me.chkAllowBalance.Location = New System.Drawing.Point(746, 32)
        Me.chkAllowBalance.Name = "chkAllowBalance"
        Me.chkAllowBalance.Size = New System.Drawing.Size(15, 14)
        Me.chkAllowBalance.TabIndex = 65
        Me.chkAllowBalance.UseVisualStyleBackColor = True
        '
        'lblTargetV
        '
        Me.lblTargetV.AutoSize = True
        Me.lblTargetV.Location = New System.Drawing.Point(230, 79)
        Me.lblTargetV.Name = "lblTargetV"
        Me.lblTargetV.Size = New System.Drawing.Size(77, 13)
        Me.lblTargetV.TabIndex = 67
        Me.lblTargetV.Text = "Target Voltage"
        Me.lblTargetV.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtTargetV
        '
        Me.txtTargetV.BackColor = System.Drawing.Color.White
        Me.txtTargetV.Location = New System.Drawing.Point(310, 75)
        Me.txtTargetV.Name = "txtTargetV"
        Me.txtTargetV.ReadOnly = True
        Me.txtTargetV.Size = New System.Drawing.Size(100, 20)
        Me.txtTargetV.TabIndex = 66
        '
        'lblPackV
        '
        Me.lblPackV.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblPackV.Location = New System.Drawing.Point(236, 54)
        Me.lblPackV.Name = "lblPackV"
        Me.lblPackV.Size = New System.Drawing.Size(71, 13)
        Me.lblPackV.TabIndex = 69
        Me.lblPackV.Text = "Pack Voltage"
        Me.lblPackV.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtPackV
        '
        Me.txtPackV.BackColor = System.Drawing.Color.White
        Me.txtPackV.Location = New System.Drawing.Point(310, 50)
        Me.txtPackV.Name = "txtPackV"
        Me.txtPackV.ReadOnly = True
        Me.txtPackV.Size = New System.Drawing.Size(100, 20)
        Me.txtPackV.TabIndex = 68
        '
        'lblCellV
        '
        Me.lblCellV.AutoSize = True
        Me.lblCellV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCellV.Location = New System.Drawing.Point(46, 31)
        Me.lblCellV.Name = "lblCellV"
        Me.lblCellV.Size = New System.Drawing.Size(94, 13)
        Me.lblCellV.TabIndex = 70
        Me.lblCellV.Text = "Cell Voltage (v)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(156, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "Balancing"
        '
        'rdoBalCB1
        '
        Me.rdoBalCB1.AutoCheck = False
        Me.rdoBalCB1.AutoSize = True
        Me.rdoBalCB1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB1.Location = New System.Drawing.Point(177, 53)
        Me.rdoBalCB1.Name = "rdoBalCB1"
        Me.rdoBalCB1.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB1.TabIndex = 73
        Me.rdoBalCB1.UseVisualStyleBackColor = True
        '
        'rdoBalCB2
        '
        Me.rdoBalCB2.AutoCheck = False
        Me.rdoBalCB2.AutoSize = True
        Me.rdoBalCB2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB2.Location = New System.Drawing.Point(177, 80)
        Me.rdoBalCB2.Name = "rdoBalCB2"
        Me.rdoBalCB2.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB2.TabIndex = 74
        Me.rdoBalCB2.UseVisualStyleBackColor = True
        '
        'rdoBalCB3
        '
        Me.rdoBalCB3.AutoCheck = False
        Me.rdoBalCB3.AutoSize = True
        Me.rdoBalCB3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB3.Location = New System.Drawing.Point(177, 106)
        Me.rdoBalCB3.Name = "rdoBalCB3"
        Me.rdoBalCB3.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB3.TabIndex = 75
        Me.rdoBalCB3.UseVisualStyleBackColor = True
        '
        'rdoBalCB4
        '
        Me.rdoBalCB4.AutoCheck = False
        Me.rdoBalCB4.AutoSize = True
        Me.rdoBalCB4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB4.Location = New System.Drawing.Point(177, 132)
        Me.rdoBalCB4.Name = "rdoBalCB4"
        Me.rdoBalCB4.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB4.TabIndex = 76
        Me.rdoBalCB4.UseVisualStyleBackColor = True
        '
        'rdoBalCB8
        '
        Me.rdoBalCB8.AutoCheck = False
        Me.rdoBalCB8.AutoSize = True
        Me.rdoBalCB8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB8.Location = New System.Drawing.Point(177, 237)
        Me.rdoBalCB8.Name = "rdoBalCB8"
        Me.rdoBalCB8.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB8.TabIndex = 80
        Me.rdoBalCB8.UseVisualStyleBackColor = True
        '
        'rdoBalCB7
        '
        Me.rdoBalCB7.AutoCheck = False
        Me.rdoBalCB7.AutoSize = True
        Me.rdoBalCB7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB7.Location = New System.Drawing.Point(177, 211)
        Me.rdoBalCB7.Name = "rdoBalCB7"
        Me.rdoBalCB7.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB7.TabIndex = 79
        Me.rdoBalCB7.UseVisualStyleBackColor = True
        '
        'rdoBalCB6
        '
        Me.rdoBalCB6.AutoCheck = False
        Me.rdoBalCB6.AutoSize = True
        Me.rdoBalCB6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB6.Location = New System.Drawing.Point(177, 185)
        Me.rdoBalCB6.Name = "rdoBalCB6"
        Me.rdoBalCB6.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB6.TabIndex = 78
        Me.rdoBalCB6.UseVisualStyleBackColor = True
        '
        'rdoBalCB5
        '
        Me.rdoBalCB5.AutoCheck = False
        Me.rdoBalCB5.AutoSize = True
        Me.rdoBalCB5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB5.Location = New System.Drawing.Point(177, 158)
        Me.rdoBalCB5.Name = "rdoBalCB5"
        Me.rdoBalCB5.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB5.TabIndex = 77
        Me.rdoBalCB5.UseVisualStyleBackColor = True
        '
        'rdoBalCB12
        '
        Me.rdoBalCB12.AutoCheck = False
        Me.rdoBalCB12.AutoSize = True
        Me.rdoBalCB12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB12.Location = New System.Drawing.Point(177, 341)
        Me.rdoBalCB12.Name = "rdoBalCB12"
        Me.rdoBalCB12.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB12.TabIndex = 84
        Me.rdoBalCB12.UseVisualStyleBackColor = True
        '
        'rdoBalCB11
        '
        Me.rdoBalCB11.AutoCheck = False
        Me.rdoBalCB11.AutoSize = True
        Me.rdoBalCB11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB11.Location = New System.Drawing.Point(177, 315)
        Me.rdoBalCB11.Name = "rdoBalCB11"
        Me.rdoBalCB11.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB11.TabIndex = 83
        Me.rdoBalCB11.UseVisualStyleBackColor = True
        '
        'rdoBalCB10
        '
        Me.rdoBalCB10.AutoCheck = False
        Me.rdoBalCB10.AutoSize = True
        Me.rdoBalCB10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB10.Location = New System.Drawing.Point(177, 289)
        Me.rdoBalCB10.Name = "rdoBalCB10"
        Me.rdoBalCB10.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB10.TabIndex = 82
        Me.rdoBalCB10.UseVisualStyleBackColor = True
        '
        'rdoBalCB9
        '
        Me.rdoBalCB9.AutoCheck = False
        Me.rdoBalCB9.AutoSize = True
        Me.rdoBalCB9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB9.Location = New System.Drawing.Point(177, 262)
        Me.rdoBalCB9.Name = "rdoBalCB9"
        Me.rdoBalCB9.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB9.TabIndex = 81
        Me.rdoBalCB9.UseVisualStyleBackColor = True
        '
        'rdoBalCB16
        '
        Me.rdoBalCB16.AutoCheck = False
        Me.rdoBalCB16.AutoSize = True
        Me.rdoBalCB16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB16.Location = New System.Drawing.Point(177, 445)
        Me.rdoBalCB16.Name = "rdoBalCB16"
        Me.rdoBalCB16.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB16.TabIndex = 88
        Me.rdoBalCB16.UseVisualStyleBackColor = True
        '
        'rdoBalCB15
        '
        Me.rdoBalCB15.AutoCheck = False
        Me.rdoBalCB15.AutoSize = True
        Me.rdoBalCB15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB15.Location = New System.Drawing.Point(177, 419)
        Me.rdoBalCB15.Name = "rdoBalCB15"
        Me.rdoBalCB15.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB15.TabIndex = 87
        Me.rdoBalCB15.UseVisualStyleBackColor = True
        '
        'rdoBalCB14
        '
        Me.rdoBalCB14.AutoCheck = False
        Me.rdoBalCB14.AutoSize = True
        Me.rdoBalCB14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB14.Location = New System.Drawing.Point(177, 393)
        Me.rdoBalCB14.Name = "rdoBalCB14"
        Me.rdoBalCB14.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB14.TabIndex = 86
        Me.rdoBalCB14.UseVisualStyleBackColor = True
        '
        'rdoBalCB13
        '
        Me.rdoBalCB13.AutoCheck = False
        Me.rdoBalCB13.AutoSize = True
        Me.rdoBalCB13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoBalCB13.Location = New System.Drawing.Point(177, 366)
        Me.rdoBalCB13.Name = "rdoBalCB13"
        Me.rdoBalCB13.Size = New System.Drawing.Size(14, 13)
        Me.rdoBalCB13.TabIndex = 85
        Me.rdoBalCB13.UseVisualStyleBackColor = True
        '
        'txtReqRate
        '
        Me.txtReqRate.BackColor = System.Drawing.Color.White
        Me.txtReqRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReqRate.Location = New System.Drawing.Point(746, 445)
        Me.txtReqRate.Name = "txtReqRate"
        Me.txtReqRate.Size = New System.Drawing.Size(39, 20)
        Me.txtReqRate.TabIndex = 90
        Me.txtReqRate.Text = "10"
        '
        'lblRequestRate
        '
        Me.lblRequestRate.AutoSize = True
        Me.lblRequestRate.Location = New System.Drawing.Point(667, 448)
        Me.lblRequestRate.Name = "lblRequestRate"
        Me.lblRequestRate.Size = New System.Drawing.Size(73, 13)
        Me.lblRequestRate.TabIndex = 89
        Me.lblRequestRate.Text = "Request Rate"
        '
        'lblHz
        '
        Me.lblHz.AutoSize = True
        Me.lblHz.Location = New System.Drawing.Point(791, 448)
        Me.lblHz.Name = "lblHz"
        Me.lblHz.Size = New System.Drawing.Size(20, 13)
        Me.lblHz.TabIndex = 91
        Me.lblHz.Text = "Hz"
        '
        'TmrRxData
        '
        Me.TmrRxData.Interval = 1
        '
        'lblCounter
        '
        Me.lblCounter.AutoSize = True
        Me.lblCounter.Location = New System.Drawing.Point(263, 205)
        Me.lblCounter.Name = "lblCounter"
        Me.lblCounter.Size = New System.Drawing.Size(44, 13)
        Me.lblCounter.TabIndex = 93
        Me.lblCounter.Text = "Counter"
        Me.lblCounter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtCounter
        '
        Me.txtCounter.BackColor = System.Drawing.Color.White
        Me.txtCounter.Location = New System.Drawing.Point(310, 201)
        Me.txtCounter.Name = "txtCounter"
        Me.txtCounter.ReadOnly = True
        Me.txtCounter.Size = New System.Drawing.Size(100, 20)
        Me.txtCounter.TabIndex = 92
        '
        'lblReadVoltDelta
        '
        Me.lblReadVoltDelta.AutoSize = True
        Me.lblReadVoltDelta.Location = New System.Drawing.Point(234, 180)
        Me.lblReadVoltDelta.Name = "lblReadVoltDelta"
        Me.lblReadVoltDelta.Size = New System.Drawing.Size(73, 13)
        Me.lblReadVoltDelta.TabIndex = 95
        Me.lblReadVoltDelta.Text = "Read Volt Diff"
        Me.lblReadVoltDelta.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtReadVoltDiff
        '
        Me.txtReadVoltDiff.BackColor = System.Drawing.Color.White
        Me.txtReadVoltDiff.Location = New System.Drawing.Point(310, 176)
        Me.txtReadVoltDiff.Name = "txtReadVoltDiff"
        Me.txtReadVoltDiff.ReadOnly = True
        Me.txtReadVoltDiff.Size = New System.Drawing.Size(100, 20)
        Me.txtReadVoltDiff.TabIndex = 94
        '
        'lblMinCellV
        '
        Me.lblMinCellV.AutoSize = True
        Me.lblMinCellV.Location = New System.Drawing.Point(224, 104)
        Me.lblMinCellV.Name = "lblMinCellV"
        Me.lblMinCellV.Size = New System.Drawing.Size(83, 13)
        Me.lblMinCellV.TabIndex = 97
        Me.lblMinCellV.Text = "Min Cell Voltage"
        '
        'txtMinCellV
        '
        Me.txtMinCellV.BackColor = System.Drawing.Color.White
        Me.txtMinCellV.Location = New System.Drawing.Point(310, 100)
        Me.txtMinCellV.Name = "txtMinCellV"
        Me.txtMinCellV.ReadOnly = True
        Me.txtMinCellV.Size = New System.Drawing.Size(100, 20)
        Me.txtMinCellV.TabIndex = 96
        '
        'lblMaxC1
        '
        Me.lblMaxC1.AutoSize = True
        Me.lblMaxC1.Location = New System.Drawing.Point(458, 53)
        Me.lblMaxC1.Name = "lblMaxC1"
        Me.lblMaxC1.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC1.TabIndex = 98
        Me.lblMaxC1.Text = "Max Status C1"
        '
        'lblMaxC2
        '
        Me.lblMaxC2.AutoSize = True
        Me.lblMaxC2.Location = New System.Drawing.Point(458, 68)
        Me.lblMaxC2.Name = "lblMaxC2"
        Me.lblMaxC2.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC2.TabIndex = 99
        Me.lblMaxC2.Text = "Max Status C2"
        '
        'lblMaxC3
        '
        Me.lblMaxC3.AutoSize = True
        Me.lblMaxC3.Location = New System.Drawing.Point(458, 83)
        Me.lblMaxC3.Name = "lblMaxC3"
        Me.lblMaxC3.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC3.TabIndex = 100
        Me.lblMaxC3.Text = "Max Status C3"
        '
        'lblMaxC4
        '
        Me.lblMaxC4.AutoSize = True
        Me.lblMaxC4.Location = New System.Drawing.Point(458, 98)
        Me.lblMaxC4.Name = "lblMaxC4"
        Me.lblMaxC4.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC4.TabIndex = 101
        Me.lblMaxC4.Text = "Max Status C4"
        '
        'lblMaxC5
        '
        Me.lblMaxC5.AutoSize = True
        Me.lblMaxC5.Location = New System.Drawing.Point(458, 113)
        Me.lblMaxC5.Name = "lblMaxC5"
        Me.lblMaxC5.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC5.TabIndex = 102
        Me.lblMaxC5.Text = "Max Status C5"
        '
        'lblMaxC6
        '
        Me.lblMaxC6.AutoSize = True
        Me.lblMaxC6.Location = New System.Drawing.Point(458, 128)
        Me.lblMaxC6.Name = "lblMaxC6"
        Me.lblMaxC6.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC6.TabIndex = 103
        Me.lblMaxC6.Text = "Max Status C6"
        '
        'lblMaxC7
        '
        Me.lblMaxC7.AutoSize = True
        Me.lblMaxC7.Location = New System.Drawing.Point(458, 143)
        Me.lblMaxC7.Name = "lblMaxC7"
        Me.lblMaxC7.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC7.TabIndex = 104
        Me.lblMaxC7.Text = "Max Status C7"
        '
        'lblMaxC8
        '
        Me.lblMaxC8.AutoSize = True
        Me.lblMaxC8.Location = New System.Drawing.Point(458, 158)
        Me.lblMaxC8.Name = "lblMaxC8"
        Me.lblMaxC8.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC8.TabIndex = 105
        Me.lblMaxC8.Text = "Max Status C8"
        '
        'lblMaxC16
        '
        Me.lblMaxC16.AutoSize = True
        Me.lblMaxC16.Location = New System.Drawing.Point(452, 278)
        Me.lblMaxC16.Name = "lblMaxC16"
        Me.lblMaxC16.Size = New System.Drawing.Size(82, 13)
        Me.lblMaxC16.TabIndex = 113
        Me.lblMaxC16.Text = "Max Status C16"
        '
        'lblMaxC15
        '
        Me.lblMaxC15.AutoSize = True
        Me.lblMaxC15.Location = New System.Drawing.Point(452, 263)
        Me.lblMaxC15.Name = "lblMaxC15"
        Me.lblMaxC15.Size = New System.Drawing.Size(82, 13)
        Me.lblMaxC15.TabIndex = 112
        Me.lblMaxC15.Text = "Max Status C15"
        '
        'lblMaxC14
        '
        Me.lblMaxC14.AutoSize = True
        Me.lblMaxC14.Location = New System.Drawing.Point(452, 248)
        Me.lblMaxC14.Name = "lblMaxC14"
        Me.lblMaxC14.Size = New System.Drawing.Size(82, 13)
        Me.lblMaxC14.TabIndex = 111
        Me.lblMaxC14.Text = "Max Status C14"
        '
        'lblMaxC13
        '
        Me.lblMaxC13.AutoSize = True
        Me.lblMaxC13.Location = New System.Drawing.Point(452, 233)
        Me.lblMaxC13.Name = "lblMaxC13"
        Me.lblMaxC13.Size = New System.Drawing.Size(82, 13)
        Me.lblMaxC13.TabIndex = 110
        Me.lblMaxC13.Text = "Max Status C13"
        '
        'lblMaxC12
        '
        Me.lblMaxC12.AutoSize = True
        Me.lblMaxC12.Location = New System.Drawing.Point(452, 218)
        Me.lblMaxC12.Name = "lblMaxC12"
        Me.lblMaxC12.Size = New System.Drawing.Size(82, 13)
        Me.lblMaxC12.TabIndex = 109
        Me.lblMaxC12.Text = "Max Status C12"
        '
        'lblMaxC11
        '
        Me.lblMaxC11.AutoSize = True
        Me.lblMaxC11.Location = New System.Drawing.Point(452, 203)
        Me.lblMaxC11.Name = "lblMaxC11"
        Me.lblMaxC11.Size = New System.Drawing.Size(82, 13)
        Me.lblMaxC11.TabIndex = 108
        Me.lblMaxC11.Text = "Max Status C11"
        '
        'lblMaxC10
        '
        Me.lblMaxC10.AutoSize = True
        Me.lblMaxC10.Location = New System.Drawing.Point(452, 188)
        Me.lblMaxC10.Name = "lblMaxC10"
        Me.lblMaxC10.Size = New System.Drawing.Size(82, 13)
        Me.lblMaxC10.TabIndex = 107
        Me.lblMaxC10.Text = "Max Status C10"
        '
        'lblMaxC9
        '
        Me.lblMaxC9.AutoSize = True
        Me.lblMaxC9.Location = New System.Drawing.Point(458, 173)
        Me.lblMaxC9.Name = "lblMaxC9"
        Me.lblMaxC9.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxC9.TabIndex = 106
        Me.lblMaxC9.Text = "Max Status C9"
        '
        'rdoMaxC1
        '
        Me.rdoMaxC1.AutoCheck = False
        Me.rdoMaxC1.AutoSize = True
        Me.rdoMaxC1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC1.Location = New System.Drawing.Point(534, 53)
        Me.rdoMaxC1.Name = "rdoMaxC1"
        Me.rdoMaxC1.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC1.TabIndex = 114
        Me.rdoMaxC1.UseVisualStyleBackColor = True
        '
        'rdoMaxC2
        '
        Me.rdoMaxC2.AutoCheck = False
        Me.rdoMaxC2.AutoSize = True
        Me.rdoMaxC2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC2.Location = New System.Drawing.Point(534, 68)
        Me.rdoMaxC2.Name = "rdoMaxC2"
        Me.rdoMaxC2.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC2.TabIndex = 115
        Me.rdoMaxC2.UseVisualStyleBackColor = True
        '
        'rdoMaxC3
        '
        Me.rdoMaxC3.AutoCheck = False
        Me.rdoMaxC3.AutoSize = True
        Me.rdoMaxC3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC3.Location = New System.Drawing.Point(534, 83)
        Me.rdoMaxC3.Name = "rdoMaxC3"
        Me.rdoMaxC3.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC3.TabIndex = 116
        Me.rdoMaxC3.UseVisualStyleBackColor = True
        '
        'rdoMaxC4
        '
        Me.rdoMaxC4.AutoCheck = False
        Me.rdoMaxC4.AutoSize = True
        Me.rdoMaxC4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC4.Location = New System.Drawing.Point(534, 98)
        Me.rdoMaxC4.Name = "rdoMaxC4"
        Me.rdoMaxC4.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC4.TabIndex = 117
        Me.rdoMaxC4.UseVisualStyleBackColor = True
        '
        'rdoMaxC5
        '
        Me.rdoMaxC5.AutoCheck = False
        Me.rdoMaxC5.AutoSize = True
        Me.rdoMaxC5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC5.Location = New System.Drawing.Point(534, 113)
        Me.rdoMaxC5.Name = "rdoMaxC5"
        Me.rdoMaxC5.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC5.TabIndex = 118
        Me.rdoMaxC5.UseVisualStyleBackColor = True
        '
        'rdoMaxC6
        '
        Me.rdoMaxC6.AutoCheck = False
        Me.rdoMaxC6.AutoSize = True
        Me.rdoMaxC6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC6.Location = New System.Drawing.Point(534, 128)
        Me.rdoMaxC6.Name = "rdoMaxC6"
        Me.rdoMaxC6.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC6.TabIndex = 119
        Me.rdoMaxC6.UseVisualStyleBackColor = True
        '
        'rdoMaxC7
        '
        Me.rdoMaxC7.AutoCheck = False
        Me.rdoMaxC7.AutoSize = True
        Me.rdoMaxC7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC7.Location = New System.Drawing.Point(534, 143)
        Me.rdoMaxC7.Name = "rdoMaxC7"
        Me.rdoMaxC7.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC7.TabIndex = 120
        Me.rdoMaxC7.UseVisualStyleBackColor = True
        '
        'rdoMaxC8
        '
        Me.rdoMaxC8.AutoCheck = False
        Me.rdoMaxC8.AutoSize = True
        Me.rdoMaxC8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC8.Location = New System.Drawing.Point(534, 158)
        Me.rdoMaxC8.Name = "rdoMaxC8"
        Me.rdoMaxC8.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC8.TabIndex = 121
        Me.rdoMaxC8.UseVisualStyleBackColor = True
        '
        'rdoMaxC9
        '
        Me.rdoMaxC9.AutoCheck = False
        Me.rdoMaxC9.AutoSize = True
        Me.rdoMaxC9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC9.Location = New System.Drawing.Point(534, 173)
        Me.rdoMaxC9.Name = "rdoMaxC9"
        Me.rdoMaxC9.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC9.TabIndex = 122
        Me.rdoMaxC9.UseVisualStyleBackColor = True
        '
        'rdoMaxC10
        '
        Me.rdoMaxC10.AutoCheck = False
        Me.rdoMaxC10.AutoSize = True
        Me.rdoMaxC10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC10.Location = New System.Drawing.Point(534, 188)
        Me.rdoMaxC10.Name = "rdoMaxC10"
        Me.rdoMaxC10.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC10.TabIndex = 123
        Me.rdoMaxC10.UseVisualStyleBackColor = True
        '
        'rdoMaxC11
        '
        Me.rdoMaxC11.AutoCheck = False
        Me.rdoMaxC11.AutoSize = True
        Me.rdoMaxC11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC11.Location = New System.Drawing.Point(534, 203)
        Me.rdoMaxC11.Name = "rdoMaxC11"
        Me.rdoMaxC11.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC11.TabIndex = 124
        Me.rdoMaxC11.UseVisualStyleBackColor = True
        '
        'rdoMaxC12
        '
        Me.rdoMaxC12.AutoCheck = False
        Me.rdoMaxC12.AutoSize = True
        Me.rdoMaxC12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC12.Location = New System.Drawing.Point(534, 218)
        Me.rdoMaxC12.Name = "rdoMaxC12"
        Me.rdoMaxC12.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC12.TabIndex = 125
        Me.rdoMaxC12.UseVisualStyleBackColor = True
        '
        'rdoMaxC13
        '
        Me.rdoMaxC13.AutoCheck = False
        Me.rdoMaxC13.AutoSize = True
        Me.rdoMaxC13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC13.Location = New System.Drawing.Point(534, 233)
        Me.rdoMaxC13.Name = "rdoMaxC13"
        Me.rdoMaxC13.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC13.TabIndex = 126
        Me.rdoMaxC13.UseVisualStyleBackColor = True
        '
        'rdoMaxC14
        '
        Me.rdoMaxC14.AutoCheck = False
        Me.rdoMaxC14.AutoSize = True
        Me.rdoMaxC14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC14.Location = New System.Drawing.Point(534, 248)
        Me.rdoMaxC14.Name = "rdoMaxC14"
        Me.rdoMaxC14.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC14.TabIndex = 127
        Me.rdoMaxC14.UseVisualStyleBackColor = True
        '
        'rdoMaxC15
        '
        Me.rdoMaxC15.AutoCheck = False
        Me.rdoMaxC15.AutoSize = True
        Me.rdoMaxC15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC15.Location = New System.Drawing.Point(534, 263)
        Me.rdoMaxC15.Name = "rdoMaxC15"
        Me.rdoMaxC15.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC15.TabIndex = 128
        Me.rdoMaxC15.UseVisualStyleBackColor = True
        '
        'rdoMaxC16
        '
        Me.rdoMaxC16.AutoCheck = False
        Me.rdoMaxC16.AutoSize = True
        Me.rdoMaxC16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxC16.Location = New System.Drawing.Point(534, 278)
        Me.rdoMaxC16.Name = "rdoMaxC16"
        Me.rdoMaxC16.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxC16.TabIndex = 129
        Me.rdoMaxC16.UseVisualStyleBackColor = True
        '
        'rdoMaxStat8
        '
        Me.rdoMaxStat8.AutoCheck = False
        Me.rdoMaxStat8.AutoSize = True
        Me.rdoMaxStat8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat8.Location = New System.Drawing.Point(534, 398)
        Me.rdoMaxStat8.Name = "rdoMaxStat8"
        Me.rdoMaxStat8.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat8.TabIndex = 146
        Me.rdoMaxStat8.UseVisualStyleBackColor = True
        '
        'rdoMaxStat7
        '
        Me.rdoMaxStat7.AutoCheck = False
        Me.rdoMaxStat7.AutoSize = True
        Me.rdoMaxStat7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat7.Location = New System.Drawing.Point(534, 383)
        Me.rdoMaxStat7.Name = "rdoMaxStat7"
        Me.rdoMaxStat7.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat7.TabIndex = 145
        Me.rdoMaxStat7.UseVisualStyleBackColor = True
        '
        'rdoMaxStat6
        '
        Me.rdoMaxStat6.AutoCheck = False
        Me.rdoMaxStat6.AutoSize = True
        Me.rdoMaxStat6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat6.Location = New System.Drawing.Point(534, 368)
        Me.rdoMaxStat6.Name = "rdoMaxStat6"
        Me.rdoMaxStat6.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat6.TabIndex = 144
        Me.rdoMaxStat6.UseVisualStyleBackColor = True
        '
        'rdoMaxStat5
        '
        Me.rdoMaxStat5.AutoCheck = False
        Me.rdoMaxStat5.AutoSize = True
        Me.rdoMaxStat5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat5.Location = New System.Drawing.Point(534, 353)
        Me.rdoMaxStat5.Name = "rdoMaxStat5"
        Me.rdoMaxStat5.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat5.TabIndex = 143
        Me.rdoMaxStat5.UseVisualStyleBackColor = True
        '
        'rdoMaxStat4
        '
        Me.rdoMaxStat4.AutoCheck = False
        Me.rdoMaxStat4.AutoSize = True
        Me.rdoMaxStat4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat4.Location = New System.Drawing.Point(534, 338)
        Me.rdoMaxStat4.Name = "rdoMaxStat4"
        Me.rdoMaxStat4.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat4.TabIndex = 142
        Me.rdoMaxStat4.UseVisualStyleBackColor = True
        '
        'rdoMaxStat3
        '
        Me.rdoMaxStat3.AutoCheck = False
        Me.rdoMaxStat3.AutoSize = True
        Me.rdoMaxStat3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat3.Location = New System.Drawing.Point(534, 323)
        Me.rdoMaxStat3.Name = "rdoMaxStat3"
        Me.rdoMaxStat3.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat3.TabIndex = 141
        Me.rdoMaxStat3.UseVisualStyleBackColor = True
        '
        'rdoMaxStat2
        '
        Me.rdoMaxStat2.AutoCheck = False
        Me.rdoMaxStat2.AutoSize = True
        Me.rdoMaxStat2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat2.Location = New System.Drawing.Point(534, 308)
        Me.rdoMaxStat2.Name = "rdoMaxStat2"
        Me.rdoMaxStat2.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat2.TabIndex = 140
        Me.rdoMaxStat2.UseVisualStyleBackColor = True
        '
        'rdoMaxStat1
        '
        Me.rdoMaxStat1.AutoCheck = False
        Me.rdoMaxStat1.AutoSize = True
        Me.rdoMaxStat1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoMaxStat1.Location = New System.Drawing.Point(534, 293)
        Me.rdoMaxStat1.Name = "rdoMaxStat1"
        Me.rdoMaxStat1.Size = New System.Drawing.Size(14, 13)
        Me.rdoMaxStat1.TabIndex = 139
        Me.rdoMaxStat1.UseVisualStyleBackColor = True
        '
        'lblMaxStat8
        '
        Me.lblMaxStat8.AutoSize = True
        Me.lblMaxStat8.Location = New System.Drawing.Point(465, 398)
        Me.lblMaxStat8.Name = "lblMaxStat8"
        Me.lblMaxStat8.Size = New System.Drawing.Size(67, 13)
        Me.lblMaxStat8.TabIndex = 138
        Me.lblMaxStat8.Text = "Max Stat OT"
        '
        'lblMaxStat7
        '
        Me.lblMaxStat7.AutoSize = True
        Me.lblMaxStat7.Location = New System.Drawing.Point(449, 383)
        Me.lblMaxStat7.Name = "lblMaxStat7"
        Me.lblMaxStat7.Size = New System.Drawing.Size(83, 13)
        Me.lblMaxStat7.TabIndex = 137
        Me.lblMaxStat7.Text = "Max Stat n_Rdy"
        '
        'lblMaxStat6
        '
        Me.lblMaxStat6.AutoSize = True
        Me.lblMaxStat6.Location = New System.Drawing.Point(445, 368)
        Me.lblMaxStat6.Name = "lblMaxStat6"
        Me.lblMaxStat6.Size = New System.Drawing.Size(87, 13)
        Me.lblMaxStat6.TabIndex = 136
        Me.lblMaxStat6.Text = "Max Stat UV_VP"
        '
        'lblMaxStat5
        '
        Me.lblMaxStat5.AutoSize = True
        Me.lblMaxStat5.Location = New System.Drawing.Point(445, 353)
        Me.lblMaxStat5.Name = "lblMaxStat5"
        Me.lblMaxStat5.Size = New System.Drawing.Size(87, 13)
        Me.lblMaxStat5.TabIndex = 135
        Me.lblMaxStat5.Text = "Max Stat UV_VA"
        '
        'lblMaxStat4
        '
        Me.lblMaxStat4.AutoSize = True
        Me.lblMaxStat4.Location = New System.Drawing.Point(454, 338)
        Me.lblMaxStat4.Name = "lblMaxStat4"
        Me.lblMaxStat4.Size = New System.Drawing.Size(78, 13)
        Me.lblMaxStat4.TabIndex = 134
        Me.lblMaxStat4.Text = "Max Stat Rev1"
        '
        'lblMaxStat3
        '
        Me.lblMaxStat3.AutoSize = True
        Me.lblMaxStat3.Location = New System.Drawing.Point(454, 323)
        Me.lblMaxStat3.Name = "lblMaxStat3"
        Me.lblMaxStat3.Size = New System.Drawing.Size(78, 13)
        Me.lblMaxStat3.TabIndex = 133
        Me.lblMaxStat3.Text = "Max Stat Rev0"
        '
        'lblMaxStat2
        '
        Me.lblMaxStat2.AutoSize = True
        Me.lblMaxStat2.Location = New System.Drawing.Point(459, 308)
        Me.lblMaxStat2.Name = "lblMaxStat2"
        Me.lblMaxStat2.Size = New System.Drawing.Size(73, 13)
        Me.lblMaxStat2.TabIndex = 132
        Me.lblMaxStat2.Text = "Max Stat OP1"
        '
        'lblMaxStat1
        '
        Me.lblMaxStat1.AutoSize = True
        Me.lblMaxStat1.Location = New System.Drawing.Point(459, 293)
        Me.lblMaxStat1.Name = "lblMaxStat1"
        Me.lblMaxStat1.Size = New System.Drawing.Size(73, 13)
        Me.lblMaxStat1.TabIndex = 131
        Me.lblMaxStat1.Text = "Max Stat OP0"
        '
        'btnPause
        '
        Me.btnPause.Location = New System.Drawing.Point(303, 363)
        Me.btnPause.Name = "btnPause"
        Me.btnPause.Size = New System.Drawing.Size(75, 23)
        Me.btnPause.TabIndex = 147
        Me.btnPause.Text = "Pause"
        Me.Tip.SetToolTip(Me.btnPause, "Allows data display to be paused and resumed.  Resume after auto pause too")
        Me.btnPause.UseVisualStyleBackColor = True
        '
        'btnAutoPause
        '
        Me.btnAutoPause.Location = New System.Drawing.Point(293, 392)
        Me.btnAutoPause.Name = "btnAutoPause"
        Me.btnAutoPause.Size = New System.Drawing.Size(95, 23)
        Me.btnAutoPause.TabIndex = 148
        Me.btnAutoPause.Text = "Auto Pause Off"
        Me.Tip.SetToolTip(Me.btnAutoPause, "When on data display is paused if any cell is above Target V or if any of the MAX" &
        "14921 status bits are set out of place")
        Me.btnAutoPause.UseVisualStyleBackColor = True
        '
        'BtnMute
        '
        Me.BtnMute.Location = New System.Drawing.Point(321, 248)
        Me.BtnMute.Name = "BtnMute"
        Me.BtnMute.Size = New System.Drawing.Size(42, 23)
        Me.BtnMute.TabIndex = 162
        Me.BtnMute.Text = "Mute"
        Me.Tip.SetToolTip(Me.BtnMute, "Mute Sound if needed")
        Me.BtnMute.UseVisualStyleBackColor = True
        '
        'pctLevelEnergy
        '
        Me.pctLevelEnergy.BackgroundImage = CType(resources.GetObject("pctLevelEnergy.BackgroundImage"), System.Drawing.Image)
        Me.pctLevelEnergy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pctLevelEnergy.InitialImage = CType(resources.GetObject("pctLevelEnergy.InitialImage"), System.Drawing.Image)
        Me.pctLevelEnergy.Location = New System.Drawing.Point(365, 443)
        Me.pctLevelEnergy.Name = "pctLevelEnergy"
        Me.pctLevelEnergy.Size = New System.Drawing.Size(101, 27)
        Me.pctLevelEnergy.TabIndex = 149
        Me.pctLevelEnergy.TabStop = False
        '
        'lblErrCounter
        '
        Me.lblErrCounter.AutoSize = True
        Me.lblErrCounter.Location = New System.Drawing.Point(671, 180)
        Me.lblErrCounter.Name = "lblErrCounter"
        Me.lblErrCounter.Size = New System.Drawing.Size(69, 13)
        Me.lblErrCounter.TabIndex = 151
        Me.lblErrCounter.Text = "Error Counter"
        Me.lblErrCounter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtErrCounter
        '
        Me.txtErrCounter.Location = New System.Drawing.Point(741, 177)
        Me.txtErrCounter.Name = "txtErrCounter"
        Me.txtErrCounter.ReadOnly = True
        Me.txtErrCounter.Size = New System.Drawing.Size(57, 20)
        Me.txtErrCounter.TabIndex = 150
        '
        'txtChkSumCalc
        '
        Me.txtChkSumCalc.Location = New System.Drawing.Point(741, 223)
        Me.txtChkSumCalc.Name = "txtChkSumCalc"
        Me.txtChkSumCalc.ReadOnly = True
        Me.txtChkSumCalc.Size = New System.Drawing.Size(43, 20)
        Me.txtChkSumCalc.TabIndex = 152
        '
        'txtChkSumRead
        '
        Me.txtChkSumRead.Location = New System.Drawing.Point(741, 200)
        Me.txtChkSumRead.Name = "txtChkSumRead"
        Me.txtChkSumRead.ReadOnly = True
        Me.txtChkSumRead.Size = New System.Drawing.Size(43, 20)
        Me.txtChkSumRead.TabIndex = 153
        '
        'lblChkSumRead
        '
        Me.lblChkSumRead.AutoSize = True
        Me.lblChkSumRead.Location = New System.Drawing.Point(664, 203)
        Me.lblChkSumRead.Name = "lblChkSumRead"
        Me.lblChkSumRead.Size = New System.Drawing.Size(76, 13)
        Me.lblChkSumRead.TabIndex = 154
        Me.lblChkSumRead.Text = "Read ChkSum"
        '
        'lblChkSumCalc
        '
        Me.lblChkSumCalc.AutoSize = True
        Me.lblChkSumCalc.Location = New System.Drawing.Point(669, 226)
        Me.lblChkSumCalc.Name = "lblChkSumCalc"
        Me.lblChkSumCalc.Size = New System.Drawing.Size(71, 13)
        Me.lblChkSumCalc.TabIndex = 155
        Me.lblChkSumCalc.Text = "Calc ChkSum"
        '
        'LblDebug
        '
        Me.LblDebug.AutoSize = True
        Me.LblDebug.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDebug.Location = New System.Drawing.Point(682, 113)
        Me.LblDebug.Name = "LblDebug"
        Me.LblDebug.Size = New System.Drawing.Size(111, 13)
        Me.LblDebug.TabIndex = 156
        Me.LblDebug.Text = "Debug Information"
        '
        'lblSerialPort
        '
        Me.lblSerialPort.AutoSize = True
        Me.lblSerialPort.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerialPort.Location = New System.Drawing.Point(700, 277)
        Me.lblSerialPort.Name = "lblSerialPort"
        Me.lblSerialPort.Size = New System.Drawing.Size(66, 13)
        Me.lblSerialPort.TabIndex = 157
        Me.lblSerialPort.Text = "Serial Port"
        '
        'lblMaxCellV
        '
        Me.lblMaxCellV.AutoSize = True
        Me.lblMaxCellV.Location = New System.Drawing.Point(221, 129)
        Me.lblMaxCellV.Name = "lblMaxCellV"
        Me.lblMaxCellV.Size = New System.Drawing.Size(86, 13)
        Me.lblMaxCellV.TabIndex = 159
        Me.lblMaxCellV.Text = "Max Cell Voltage"
        '
        'txtMaxCellV
        '
        Me.txtMaxCellV.BackColor = System.Drawing.Color.White
        Me.txtMaxCellV.Location = New System.Drawing.Point(310, 125)
        Me.txtMaxCellV.Name = "txtMaxCellV"
        Me.txtMaxCellV.ReadOnly = True
        Me.txtMaxCellV.Size = New System.Drawing.Size(100, 20)
        Me.txtMaxCellV.TabIndex = 158
        '
        'chkAllowCharge
        '
        Me.chkAllowCharge.AutoSize = True
        Me.chkAllowCharge.Location = New System.Drawing.Point(746, 50)
        Me.chkAllowCharge.Name = "chkAllowCharge"
        Me.chkAllowCharge.Size = New System.Drawing.Size(15, 14)
        Me.chkAllowCharge.TabIndex = 161
        Me.chkAllowCharge.UseVisualStyleBackColor = True
        '
        'lblAllowCharge
        '
        Me.lblAllowCharge.AutoSize = True
        Me.lblAllowCharge.Location = New System.Drawing.Point(632, 50)
        Me.lblAllowCharge.Name = "lblAllowCharge"
        Me.lblAllowCharge.Size = New System.Drawing.Size(115, 13)
        Me.lblAllowCharge.TabIndex = 160
        Me.lblAllowCharge.Text = "Allow Auto (Dis)Charge"
        '
        'TmrSafetyBing
        '
        Me.TmrSafetyBing.Interval = 1000
        '
        'rdoCharge
        '
        Me.rdoCharge.AutoCheck = False
        Me.rdoCharge.AutoSize = True
        Me.rdoCharge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoCharge.Location = New System.Drawing.Point(741, 145)
        Me.rdoCharge.Name = "rdoCharge"
        Me.rdoCharge.Size = New System.Drawing.Size(14, 13)
        Me.rdoCharge.TabIndex = 164
        Me.rdoCharge.UseVisualStyleBackColor = True
        '
        'lblCharge
        '
        Me.lblCharge.AutoSize = True
        Me.lblCharge.Location = New System.Drawing.Point(689, 144)
        Me.lblCharge.Name = "lblCharge"
        Me.lblCharge.Size = New System.Drawing.Size(49, 13)
        Me.lblCharge.TabIndex = 163
        Me.lblCharge.Text = "Charging"
        '
        'rdoDischarge
        '
        Me.rdoDischarge.AutoCheck = False
        Me.rdoDischarge.AutoSize = True
        Me.rdoDischarge.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoDischarge.Location = New System.Drawing.Point(741, 161)
        Me.rdoDischarge.Name = "rdoDischarge"
        Me.rdoDischarge.Size = New System.Drawing.Size(14, 13)
        Me.rdoDischarge.TabIndex = 166
        Me.rdoDischarge.UseVisualStyleBackColor = True
        '
        'lblDisCharge
        '
        Me.lblDisCharge.AutoSize = True
        Me.lblDisCharge.Location = New System.Drawing.Point(683, 159)
        Me.lblDisCharge.Name = "lblDisCharge"
        Me.lblDisCharge.Size = New System.Drawing.Size(55, 13)
        Me.lblDisCharge.TabIndex = 165
        Me.lblDisCharge.Text = "Discharge"
        '
        'rdoChargePause
        '
        Me.rdoChargePause.AutoCheck = False
        Me.rdoChargePause.AutoSize = True
        Me.rdoChargePause.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdoChargePause.Location = New System.Drawing.Point(741, 130)
        Me.rdoChargePause.Name = "rdoChargePause"
        Me.rdoChargePause.Size = New System.Drawing.Size(14, 13)
        Me.rdoChargePause.TabIndex = 168
        Me.rdoChargePause.UseVisualStyleBackColor = True
        '
        'lblChargePause
        '
        Me.lblChargePause.AutoSize = True
        Me.lblChargePause.Location = New System.Drawing.Point(658, 129)
        Me.lblChargePause.Name = "lblChargePause"
        Me.lblChargePause.Size = New System.Drawing.Size(80, 13)
        Me.lblChargePause.TabIndex = 167
        Me.lblChargePause.Text = "Charge Paused"
        '
        'BtnSaveFile
        '
        Me.BtnSaveFile.Location = New System.Drawing.Point(303, 293)
        Me.BtnSaveFile.Name = "BtnSaveFile"
        Me.BtnSaveFile.Size = New System.Drawing.Size(75, 23)
        Me.BtnSaveFile.TabIndex = 169
        Me.BtnSaveFile.Text = "File"
        Me.BtnSaveFile.UseVisualStyleBackColor = True
        '
        'BtnLog
        '
        Me.BtnLog.Location = New System.Drawing.Point(298, 318)
        Me.BtnLog.Name = "BtnLog"
        Me.BtnLog.Size = New System.Drawing.Size(84, 23)
        Me.BtnLog.TabIndex = 170
        Me.BtnLog.Text = "Logging On"
        Me.BtnLog.UseVisualStyleBackColor = True
        '
        'lblCellVSpread
        '
        Me.lblCellVSpread.AutoSize = True
        Me.lblCellVSpread.Location = New System.Drawing.Point(207, 154)
        Me.lblCellVSpread.Name = "lblCellVSpread"
        Me.lblCellVSpread.Size = New System.Drawing.Size(100, 13)
        Me.lblCellVSpread.TabIndex = 172
        Me.lblCellVSpread.Text = "Cell Voltage Spread"
        Me.lblCellVSpread.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtCellVSpread
        '
        Me.txtCellVSpread.BackColor = System.Drawing.Color.White
        Me.txtCellVSpread.Location = New System.Drawing.Point(310, 150)
        Me.txtCellVSpread.Name = "txtCellVSpread"
        Me.txtCellVSpread.ReadOnly = True
        Me.txtCellVSpread.Size = New System.Drawing.Size(100, 20)
        Me.txtCellVSpread.TabIndex = 171
        '
        'FrmMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(831, 476)
        Me.Controls.Add(Me.lblCellVSpread)
        Me.Controls.Add(Me.txtCellVSpread)
        Me.Controls.Add(Me.BtnLog)
        Me.Controls.Add(Me.BtnSaveFile)
        Me.Controls.Add(Me.rdoChargePause)
        Me.Controls.Add(Me.lblChargePause)
        Me.Controls.Add(Me.rdoDischarge)
        Me.Controls.Add(Me.lblDisCharge)
        Me.Controls.Add(Me.rdoCharge)
        Me.Controls.Add(Me.lblCharge)
        Me.Controls.Add(Me.BtnMute)
        Me.Controls.Add(Me.chkAllowCharge)
        Me.Controls.Add(Me.lblAllowCharge)
        Me.Controls.Add(Me.lblMaxCellV)
        Me.Controls.Add(Me.txtMaxCellV)
        Me.Controls.Add(Me.lblSerialPort)
        Me.Controls.Add(Me.LblDebug)
        Me.Controls.Add(Me.lblChkSumCalc)
        Me.Controls.Add(Me.lblChkSumRead)
        Me.Controls.Add(Me.txtChkSumRead)
        Me.Controls.Add(Me.txtChkSumCalc)
        Me.Controls.Add(Me.lblErrCounter)
        Me.Controls.Add(Me.txtErrCounter)
        Me.Controls.Add(Me.pctLevelEnergy)
        Me.Controls.Add(Me.btnAutoPause)
        Me.Controls.Add(Me.btnPause)
        Me.Controls.Add(Me.rdoMaxStat8)
        Me.Controls.Add(Me.rdoMaxStat7)
        Me.Controls.Add(Me.rdoMaxStat6)
        Me.Controls.Add(Me.rdoMaxStat5)
        Me.Controls.Add(Me.rdoMaxStat4)
        Me.Controls.Add(Me.rdoMaxStat3)
        Me.Controls.Add(Me.rdoMaxStat2)
        Me.Controls.Add(Me.rdoMaxStat1)
        Me.Controls.Add(Me.lblMaxStat8)
        Me.Controls.Add(Me.lblMaxStat7)
        Me.Controls.Add(Me.lblMaxStat6)
        Me.Controls.Add(Me.lblMaxStat5)
        Me.Controls.Add(Me.lblMaxStat4)
        Me.Controls.Add(Me.lblMaxStat3)
        Me.Controls.Add(Me.lblMaxStat2)
        Me.Controls.Add(Me.lblMaxStat1)
        Me.Controls.Add(Me.rdoMaxC16)
        Me.Controls.Add(Me.rdoMaxC15)
        Me.Controls.Add(Me.rdoMaxC14)
        Me.Controls.Add(Me.rdoMaxC13)
        Me.Controls.Add(Me.rdoMaxC12)
        Me.Controls.Add(Me.rdoMaxC11)
        Me.Controls.Add(Me.rdoMaxC10)
        Me.Controls.Add(Me.rdoMaxC9)
        Me.Controls.Add(Me.rdoMaxC8)
        Me.Controls.Add(Me.rdoMaxC7)
        Me.Controls.Add(Me.rdoMaxC6)
        Me.Controls.Add(Me.rdoMaxC5)
        Me.Controls.Add(Me.rdoMaxC4)
        Me.Controls.Add(Me.rdoMaxC3)
        Me.Controls.Add(Me.rdoMaxC2)
        Me.Controls.Add(Me.rdoMaxC1)
        Me.Controls.Add(Me.lblMaxC16)
        Me.Controls.Add(Me.lblMaxC15)
        Me.Controls.Add(Me.lblMaxC14)
        Me.Controls.Add(Me.lblMaxC13)
        Me.Controls.Add(Me.lblMaxC12)
        Me.Controls.Add(Me.lblMaxC11)
        Me.Controls.Add(Me.lblMaxC10)
        Me.Controls.Add(Me.lblMaxC9)
        Me.Controls.Add(Me.lblMaxC8)
        Me.Controls.Add(Me.lblMaxC7)
        Me.Controls.Add(Me.lblMaxC6)
        Me.Controls.Add(Me.lblMaxC5)
        Me.Controls.Add(Me.lblMaxC4)
        Me.Controls.Add(Me.lblMaxC3)
        Me.Controls.Add(Me.lblMaxC2)
        Me.Controls.Add(Me.lblMaxC1)
        Me.Controls.Add(Me.lblMinCellV)
        Me.Controls.Add(Me.txtMinCellV)
        Me.Controls.Add(Me.lblReadVoltDelta)
        Me.Controls.Add(Me.txtReadVoltDiff)
        Me.Controls.Add(Me.lblCounter)
        Me.Controls.Add(Me.txtCounter)
        Me.Controls.Add(Me.lblHz)
        Me.Controls.Add(Me.txtReqRate)
        Me.Controls.Add(Me.lblRequestRate)
        Me.Controls.Add(Me.rdoBalCB16)
        Me.Controls.Add(Me.rdoBalCB15)
        Me.Controls.Add(Me.rdoBalCB14)
        Me.Controls.Add(Me.rdoBalCB13)
        Me.Controls.Add(Me.rdoBalCB12)
        Me.Controls.Add(Me.rdoBalCB11)
        Me.Controls.Add(Me.rdoBalCB10)
        Me.Controls.Add(Me.rdoBalCB9)
        Me.Controls.Add(Me.rdoBalCB8)
        Me.Controls.Add(Me.rdoBalCB7)
        Me.Controls.Add(Me.rdoBalCB6)
        Me.Controls.Add(Me.rdoBalCB5)
        Me.Controls.Add(Me.rdoBalCB4)
        Me.Controls.Add(Me.rdoBalCB3)
        Me.Controls.Add(Me.rdoBalCB2)
        Me.Controls.Add(Me.rdoBalCB1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCellV)
        Me.Controls.Add(Me.lblPackV)
        Me.Controls.Add(Me.txtPackV)
        Me.Controls.Add(Me.lblTargetV)
        Me.Controls.Add(Me.txtTargetV)
        Me.Controls.Add(Me.chkAllowBalance)
        Me.Controls.Add(Me.TxtVoltDiff)
        Me.Controls.Add(Me.lblVoltDiff)
        Me.Controls.Add(Me.lblAllowBalce)
        Me.Controls.Add(Me.lblTxData)
        Me.Controls.Add(Me.LblRxData)
        Me.Controls.Add(Me.LblCB16)
        Me.Controls.Add(Me.LblCB15)
        Me.Controls.Add(Me.LblCB14)
        Me.Controls.Add(Me.LblCB13)
        Me.Controls.Add(Me.LblCB12)
        Me.Controls.Add(Me.LblCB11)
        Me.Controls.Add(Me.LblCB10)
        Me.Controls.Add(Me.LblCB9)
        Me.Controls.Add(Me.LblCB8)
        Me.Controls.Add(Me.LblCB7)
        Me.Controls.Add(Me.LblCB6)
        Me.Controls.Add(Me.LblCB5)
        Me.Controls.Add(Me.LblCB4)
        Me.Controls.Add(Me.LblCB3)
        Me.Controls.Add(Me.LblCB2)
        Me.Controls.Add(Me.LblCB1)
        Me.Controls.Add(Me.TxtCB16)
        Me.Controls.Add(Me.TxtCB15)
        Me.Controls.Add(Me.TxtCB14)
        Me.Controls.Add(Me.TxtCB13)
        Me.Controls.Add(Me.TxtCB12)
        Me.Controls.Add(Me.TxtCB11)
        Me.Controls.Add(Me.TxtCB10)
        Me.Controls.Add(Me.TxtCB9)
        Me.Controls.Add(Me.TxtCB8)
        Me.Controls.Add(Me.TxtCB7)
        Me.Controls.Add(Me.TxtCB6)
        Me.Controls.Add(Me.TxtCB5)
        Me.Controls.Add(Me.TxtCB4)
        Me.Controls.Add(Me.TxtCB3)
        Me.Controls.Add(Me.TxtCB2)
        Me.Controls.Add(Me.TxtCB1)
        Me.Controls.Add(Me.CmdGetData)
        Me.Controls.Add(Me.BtnComPrt)
        Me.Controls.Add(Me.CmbBaudRate)
        Me.Controls.Add(Me.TxtBaudRate)
        Me.Controls.Add(Me.TxtSerialPrt)
        Me.Controls.Add(Me.CmbSerialPorts)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmMain"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Battery Module"
        CType(Me.pctLevelEnergy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents CmbSerialPorts As ComboBox
    Friend WithEvents TxtSerialPrt As TextBox
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents TxtBaudRate As TextBox
    Friend WithEvents CmbBaudRate As ComboBox
    Friend WithEvents BtnComPrt As Button
    Friend WithEvents CmdGetData As Button
    Friend WithEvents TxtCB1 As TextBox
    Friend WithEvents TxtCB2 As TextBox
    Friend WithEvents TxtCB3 As TextBox
    Friend WithEvents TxtCB4 As TextBox
    Friend WithEvents TxtCB5 As TextBox
    Friend WithEvents TxtCB10 As TextBox
    Friend WithEvents TxtCB9 As TextBox
    Friend WithEvents TxtCB8 As TextBox
    Friend WithEvents TxtCB7 As TextBox
    Friend WithEvents TxtCB6 As TextBox
    Friend WithEvents TxtCB15 As TextBox
    Friend WithEvents TxtCB14 As TextBox
    Friend WithEvents TxtCB13 As TextBox
    Friend WithEvents TxtCB12 As TextBox
    Friend WithEvents TxtCB11 As TextBox
    Friend WithEvents TxtCB16 As TextBox
    Friend WithEvents TmrSendData As Timer
    Friend WithEvents LblCB1 As Label
    Friend WithEvents LblCB2 As Label
    Friend WithEvents LblCB4 As Label
    Friend WithEvents LblCB3 As Label
    Friend WithEvents LblCB8 As Label
    Friend WithEvents LblCB7 As Label
    Friend WithEvents LblCB6 As Label
    Friend WithEvents LblCB5 As Label
    Friend WithEvents LblCB12 As Label
    Friend WithEvents LblCB11 As Label
    Friend WithEvents LblCB10 As Label
    Friend WithEvents LblCB9 As Label
    Friend WithEvents LblCB16 As Label
    Friend WithEvents LblCB15 As Label
    Friend WithEvents LblCB14 As Label
    Friend WithEvents LblCB13 As Label
    Friend WithEvents LblRxData As Label
    Friend WithEvents lblTxData As Label
    Friend WithEvents lblAllowBalce As Label
    Friend WithEvents lblVoltDiff As Label
    Friend WithEvents TxtVoltDiff As TextBox
    Friend WithEvents chkAllowBalance As CheckBox
    Friend WithEvents lblTargetV As Label
    Friend WithEvents txtTargetV As TextBox
    Friend WithEvents lblPackV As Label
    Friend WithEvents txtPackV As TextBox
    Friend WithEvents lblCellV As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents rdoBalCB1 As RadioButton
    Friend WithEvents rdoBalCB2 As RadioButton
    Friend WithEvents rdoBalCB3 As RadioButton
    Friend WithEvents rdoBalCB4 As RadioButton
    Friend WithEvents rdoBalCB8 As RadioButton
    Friend WithEvents rdoBalCB7 As RadioButton
    Friend WithEvents rdoBalCB6 As RadioButton
    Friend WithEvents rdoBalCB5 As RadioButton
    Friend WithEvents rdoBalCB12 As RadioButton
    Friend WithEvents rdoBalCB11 As RadioButton
    Friend WithEvents rdoBalCB10 As RadioButton
    Friend WithEvents rdoBalCB9 As RadioButton
    Friend WithEvents rdoBalCB16 As RadioButton
    Friend WithEvents rdoBalCB15 As RadioButton
    Friend WithEvents rdoBalCB14 As RadioButton
    Friend WithEvents rdoBalCB13 As RadioButton
    Friend WithEvents txtReqRate As TextBox
    Friend WithEvents lblRequestRate As Label
    Friend WithEvents lblHz As Label
    Friend WithEvents TmrRxData As Timer
    Friend WithEvents lblCounter As Label
    Friend WithEvents txtCounter As TextBox
    Friend WithEvents lblReadVoltDelta As Label
    Friend WithEvents txtReadVoltDiff As TextBox
    Friend WithEvents lblMinCellV As Label
    Friend WithEvents txtMinCellV As TextBox
    Friend WithEvents lblMaxC1 As Label
    Friend WithEvents lblMaxC2 As Label
    Friend WithEvents lblMaxC3 As Label
    Friend WithEvents lblMaxC4 As Label
    Friend WithEvents lblMaxC5 As Label
    Friend WithEvents lblMaxC6 As Label
    Friend WithEvents lblMaxC7 As Label
    Friend WithEvents lblMaxC8 As Label
    Friend WithEvents lblMaxC16 As Label
    Friend WithEvents lblMaxC15 As Label
    Friend WithEvents lblMaxC14 As Label
    Friend WithEvents lblMaxC13 As Label
    Friend WithEvents lblMaxC12 As Label
    Friend WithEvents lblMaxC11 As Label
    Friend WithEvents lblMaxC10 As Label
    Friend WithEvents lblMaxC9 As Label
    Friend WithEvents rdoMaxC1 As RadioButton
    Friend WithEvents rdoMaxC2 As RadioButton
    Friend WithEvents rdoMaxC3 As RadioButton
    Friend WithEvents rdoMaxC4 As RadioButton
    Friend WithEvents rdoMaxC5 As RadioButton
    Friend WithEvents rdoMaxC6 As RadioButton
    Friend WithEvents rdoMaxC7 As RadioButton
    Friend WithEvents rdoMaxC8 As RadioButton
    Friend WithEvents rdoMaxC9 As RadioButton
    Friend WithEvents rdoMaxC10 As RadioButton
    Friend WithEvents rdoMaxC11 As RadioButton
    Friend WithEvents rdoMaxC12 As RadioButton
    Friend WithEvents rdoMaxC13 As RadioButton
    Friend WithEvents rdoMaxC14 As RadioButton
    Friend WithEvents rdoMaxC15 As RadioButton
    Friend WithEvents rdoMaxC16 As RadioButton
    Friend WithEvents rdoMaxStat8 As RadioButton
    Friend WithEvents rdoMaxStat7 As RadioButton
    Friend WithEvents rdoMaxStat6 As RadioButton
    Friend WithEvents rdoMaxStat5 As RadioButton
    Friend WithEvents rdoMaxStat4 As RadioButton
    Friend WithEvents rdoMaxStat3 As RadioButton
    Friend WithEvents rdoMaxStat2 As RadioButton
    Friend WithEvents rdoMaxStat1 As RadioButton
    Friend WithEvents lblMaxStat8 As Label
    Friend WithEvents lblMaxStat7 As Label
    Friend WithEvents lblMaxStat6 As Label
    Friend WithEvents lblMaxStat5 As Label
    Friend WithEvents lblMaxStat4 As Label
    Friend WithEvents lblMaxStat3 As Label
    Friend WithEvents lblMaxStat2 As Label
    Friend WithEvents lblMaxStat1 As Label
    Friend WithEvents btnPause As Button
    Friend WithEvents btnAutoPause As Button
    Friend WithEvents Tip As ToolTip
    Friend WithEvents pctLevelEnergy As PictureBox
    Friend WithEvents lblErrCounter As Label
    Friend WithEvents txtErrCounter As TextBox
    Friend WithEvents txtChkSumCalc As TextBox
    Friend WithEvents txtChkSumRead As TextBox
    Friend WithEvents lblChkSumRead As Label
    Friend WithEvents lblChkSumCalc As Label
    Friend WithEvents LblDebug As Label
    Friend WithEvents lblSerialPort As Label
    Friend WithEvents lblMaxCellV As Label
    Friend WithEvents txtMaxCellV As TextBox
    Friend WithEvents chkAllowCharge As CheckBox
    Friend WithEvents lblAllowCharge As Label
    Friend WithEvents TmrSafetyBing As Timer
    Friend WithEvents BtnMute As Button
    Friend WithEvents rdoCharge As RadioButton
    Friend WithEvents lblCharge As Label
    Friend WithEvents rdoDischarge As RadioButton
    Friend WithEvents lblDisCharge As Label
    Friend WithEvents rdoChargePause As RadioButton
    Friend WithEvents lblChargePause As Label
    Friend WithEvents SaveLogFileDlg As SaveFileDialog
    Friend WithEvents BtnSaveFile As Button
    Friend WithEvents BtnLog As Button
    Friend WithEvents lblCellVSpread As Label
    Friend WithEvents txtCellVSpread As TextBox
End Class
